const TAKO_SIZE = 400.0;
const TAKO_MAX_DOWNWARD_Y_VELOCITY = -4000.0;
const CROWBAR_SIZE = 200.0;
const CROWBAR_SPEED = 5000.0;
const GRAVITY = 6000.0;
const TAKO_SPEED = 2000.0;
const TAKO_JUMP_VELOCITY = 4500.0;
const ARENA_SIZE_X = 4000.0;
const ARENA_SIZE_Y = 3000.0;
const SHOOT_ANGLE_MINIMUM = 40.0;
const SHOOT_ANGLE_MAXIMUM = 80.0;
const NAME_CHAR_LIMIT = 15;

const app = new PIXI.Application({
    resolution: window.devicePixelRatio, autoDensity: true, backgroundColor: 0xFFFFFF
});

let timeSinceLastUpdate = 0.0;
function get_delta_milliseconds() {
    const deltaMilliSeconds = performance.now() - timeSinceLastUpdate;
    timeSinceLastUpdate = performance.now();
    return deltaMilliSeconds;
}

let fontsLoaded = false;
let texturesLoaded = false;

function fonts_loaded() {
    fontsLoaded = true;
    if(fontsLoaded && texturesLoaded) {
        mainMenu.start();
    }
}

function textures_loaded() {
    texturesLoaded = true;
    if(fontsLoaded && texturesLoaded) {
        mainMenu.start();
    }
}


/* Loading webfont segment from: https://pixijs.io/examples/#/text/webfont.js */
window.WebFontConfig = {
    google: {
        families: ['Indie Flower'],
    },

    active() {
        fonts_loaded();
    },
};
/* eslint-disable */
// include the web-font loader script
(function() {
    const wf = document.createElement('script');
    wf.src = `${document.location.protocol === 'https:' ? 'https' : 'http'
    }://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js`;
    wf.type = 'text/javascript';
    wf.async = 'true';
    const s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
}());
/* eslint-enabled */

let textures = {};

const loader = new PIXI.Loader();

let numberOfTexturesLoaded = 0;
let numberOfTexturesToLoad = 0;
function loader_animation_add(spriteName, numberOfFrames, msBetweenFrames, flipTexture = false) {
    textures[spriteName] = [];
    for(let i = 0; i < numberOfFrames; i++) {
        numberOfTexturesToLoad += 1;
        loader.add(spriteName + i, 'assets/' + spriteName + i.toString().padStart(4, '0') + ".png");
        textures[spriteName].push({ texture: null, time: msBetweenFrames, flip: flipTexture });
    }
}

loader_animation_add('background', 1, 0, false);
loader_animation_add('shooticon', 1, 0, false);
loader_animation_add('wasd', 1, 0, false);
loader_animation_add('space', 1, 0, false);
loader_animation_add('close', 1, 0, false);
loader_animation_add('takoidle', 5, 83.0);
loader_animation_add('takowalk', 5, 83.0);
loader_animation_add('takocrowbarwalk', 5, 83.0);
loader_animation_add('takocrowbaridle', 5, 83.0);
loader_animation_add('takobonked', 9, 83.0);
loader_animation_add('takomelting', 9, 83.0);
loader_animation_add('shootingangle', 6, 83.0);
loader_animation_add('inaback', 3, 125.0);
loader_animation_add('inaeyes', 3, 125.0);
loader_animation_add('inafront', 3, 125.0);
loader_animation_add('crowbar', 8, 56.0);
loader_animation_add('crowbarstationary', 3, 125.0);
loader_animation_add('table', 3, 125.0);
loader_animation_add('X', 3, 125.0);
loader_animation_add('O', 3, 125.0);
loader_animation_add('selectsquare', 3, 125.0);
loader_animation_add('winline', 3, 125.0);
loader_animation_add('tictactoeboard', 3, 125.0);

loader.load((loader, resources) => {
    for(let [key, value] of Object.entries(textures)) {
        for(let i = 0; i < value.length; i++) {
            value[i].texture = resources[key + i].texture;
            if(value[i].flip) {
                value[i].texture.rotate = PIXI.groupD8.MIRROR_VERTICAL; // Flip texture
            }
        }
    }
    textures_loaded();
});

loader.onProgress.add(() => {
    numberOfTexturesLoaded += 1;
    document.getElementById("loadingBarFront").style.width = (numberOfTexturesLoaded / numberOfTexturesToLoad) * 100 + "%";
});

function transform_container_to_world_coordinates(containerToTransform) {
    const appScale = app.screen.height/ARENA_SIZE_Y;
    const arenaRenderedXSize = (appScale * ARENA_SIZE_X) / 2;
    containerToTransform.setTransform(app.screen.width / 2 - arenaRenderedXSize, 0, appScale, appScale);
}

function transform_tiling_background(backgroundToTransform) {
    const appScale = app.screen.height/ARENA_SIZE_Y;
    const arenaRenderedXSize = (appScale * ARENA_SIZE_X) / 2;
    backgroundToTransform.width = app.screen.width;
    backgroundToTransform.height = app.screen.height;
    backgroundToTransform.tileScale.x = appScale;
    backgroundToTransform.tileScale.y = appScale;
    backgroundToTransform.tilePosition.x = app.screen.width / 2 - arenaRenderedXSize;
}

let mainMenu = {
    MASTER_SERVER_ADDRESS: "tictacbonk.io:50135",
    TIME_TO_SHOW_MENU: 500.0,
    menuTime: 0.0,
    TIME_TO_REMOVE_LOADING_BAR: 100.0,
    loadingBarTime: 100.0,
    loadingBarInitialHeight: 40,
    textboxName: "",
    serverListSocket: null,
    socketConnected: false,
    TIME_BETWEEN_REFRESH: 2000.0,
    timeSinceLastRefresh: 0.0,
    firstMessage: true,
    menuHeight: 70,
    serverListData: [],
    start: function() {
        timeSinceLastUpdate = performance.now();
        app.ticker.add(mainMenu.loop);
        mainMenu.setup_socket();
    },
    setup_socket: function() {
        mainMenu.serverListSocket = new WebSocket("wss://" + mainMenu.MASTER_SERVER_ADDRESS);
        mainMenu.serverListSocket.binaryType = "arraybuffer";
        mainMenu.serverListSocket.onopen = function(event) {
            console.log("Connection established with master server");
            mainMenu.socketConnected = true;
        };
        mainMenu.serverListSocket.onmessage = function(event) {
            let frame = {
                data: event.data,
                offset: 0
            };
            const get_right_text = function(numOfPlayers, maxNumOfPlayers) {
                if(numOfPlayers < 0) {
                    return "OFFLINE";
                }
                else if(numOfPlayers >= maxNumOfPlayers) {
                    return "FULL";
                }
                return numOfPlayers + "/" + maxNumOfPlayers;
            };
            const set_button_disabled = function(buttonElement, buttonRightTextString) {
                buttonElement.disabled = (buttonRightTextString === "FULL" || buttonRightTextString === "OFFLINE");
            }
            const serverListElement = document.getElementById("serverList");
            if(mainMenu.firstMessage) {
                const numberOfServers = read_uint16_from_data(frame);
                document.getElementById("connectToMasterMessage").style.display = "none";
                for(let i = 0; i < numberOfServers; i++) {
                    mainMenu.serverListData.push({});
                    mainMenu.serverListData[i].name = read_string_from_data(frame);
                    mainMenu.serverListData[i].ip = read_string_from_data(frame);
                    mainMenu.serverListData[i].numOfPlayers = read_int16_from_data(frame);
                    mainMenu.serverListData[i].maxNumOfPlayers = read_int16_from_data(frame);
                    
                    const buttonElement = document.createElement("button");
                    buttonElement.style.textAlign = "left";
                    buttonElement.id = "serverButton" + i;
                    buttonElement.onclick = function() { mainMenu.connect_to_index(i); }
                    buttonElement.classList.add("serverButton");
                    const buttonLeftText = document.createTextNode(mainMenu.serverListData[i].name);
                    buttonElement.appendChild(buttonLeftText);
                    const buttonRightSpan = document.createElement("span");
                    buttonRightSpan.style.float = "right";
                    buttonRightSpan.id = "serverButtonRightSpan" + i;
                    const buttonRightTextString = get_right_text(mainMenu.serverListData[i].numOfPlayers, mainMenu.serverListData[i].maxNumOfPlayers);
                    set_button_disabled(buttonElement, buttonRightTextString);
                    const buttonRightText = document.createTextNode(buttonRightTextString);
                    buttonRightSpan.appendChild(buttonRightText);
                    buttonElement.appendChild(buttonRightSpan);
                    serverListElement.appendChild(buttonElement);
                }
                mainMenu.firstMessage = false;
            }
            else {
                for(let i = 0; i < mainMenu.serverListData.length; i++) {
                    mainMenu.serverListData[i].numOfPlayers = read_int16_from_data(frame);
                    mainMenu.serverListData[i].maxNumOfPlayers = read_int16_from_data(frame);
                    const buttonRightSpan = document.getElementById("serverButtonRightSpan" + i);
                    const buttonRightTextString = get_right_text(mainMenu.serverListData[i].numOfPlayers, mainMenu.serverListData[i].maxNumOfPlayers);
                    buttonRightSpan.textContent = buttonRightTextString;
                    set_button_disabled(document.getElementById("serverButton" + i), buttonRightTextString);
                }
            }
        };
        mainMenu.serverListSocket.onclose = function(event) {
            document.getElementById("serverList").style.display = "none";
            let masterMessage = document.getElementById("serverTitle");
            masterMessage.textContent = "Connection with server list closed. Refresh webpage.";
            console.log("Connection closed with master server");
        };
        mainMenu.serverListSocket.onerror = function(error) {
            document.getElementById("serverList").style.display = "none";
            let masterMessage = document.getElementById("serverTitle");
            masterMessage.textContent = "Connection with server list closed. Refresh webpage.";
            console.log("Connection error with master server: " + error.data);
        };
    },
    connect_to_index: function(serverIndex) {
        document.getElementById("serverList").style.display = "none";
        let connectingMessageElement = document.getElementById("connectingMessage");
        connectingMessageElement.style.display = "flex";
        connectingMessageElement.textContent = "Connecting to " + mainMenu.serverListData[serverIndex].name + "...";
        start_game(mainMenu.serverListData[serverIndex].ip);
    },
    loop: function() {
        const deltaMilliSeconds = get_delta_milliseconds();
        
        if(mainMenu.socketConnected) {
            mainMenu.timeSinceLastRefresh += deltaMilliSeconds;
            if(mainMenu.timeSinceLastRefresh >= mainMenu.TIME_BETWEEN_REFRESH) {
                mainMenu.timeSinceLastRefresh = 0;
                mainMenu.serverListSocket.send("r");
            }
        }
        
        if(mainMenu.loadingBarTime === 0 && mainMenu.menuTime < mainMenu.TIME_TO_SHOW_MENU) {
            mainMenu.menuTime += deltaMilliSeconds;
            if(mainMenu.menuTime >= mainMenu.TIME_TO_SHOW_MENU) {
                mainMenu.menuTime = mainMenu.TIME_TO_SHOW_MENU;
            }
            if(mainMenu.menuTime <= mainMenu.TIME_TO_SHOW_MENU) {
                document.getElementById("menu").style.height = mainMenu.menuHeight * (mainMenu.menuTime / mainMenu.TIME_TO_SHOW_MENU) + "%";
            }
        }
        
        if(mainMenu.loadingBarTime > 0) {
            mainMenu.loadingBarTime -= deltaMilliSeconds;
            if(mainMenu.loadingBarTime <= 0) {
                mainMenu.loadingBarTime = 0;
            }
            if(mainMenu.loadingBarTime >= 0) {
                document.getElementById("loadingBarFront").style.height = mainMenu.loadingBarInitialHeight * (mainMenu.loadingBarTime / mainMenu.TIME_TO_REMOVE_LOADING_BAR) + "px";
            }
            if(mainMenu.loadingBarTime === 0) {
                document.getElementById("menu").style.display = "flex";
            }
        }
        
        const nameboxElement = document.getElementById("namebox");
        const nameboxTextArray = Array.from(nameboxElement.value);
        if(nameboxTextArray.length > NAME_CHAR_LIMIT) {
            nameboxElement.value = nameboxTextArray.slice(0, NAME_CHAR_LIMIT).join('');
        }
        mainMenu.textboxName = nameboxElement.value.trim();
        document.getElementById("divToShowAfterCheck").style.display = (mainMenu.textboxName.length > 0) ? "flex" : "none";
    }
};

let ticTacToe = {
    BOARD_SIZE: 1300.0,
    BOARD_Y_POS: 550.0,
    boardContainer: new PIXI.Container(),
    announcementContainer: new PIXI.Container(),
    boardData: [],
    slotContainers: [],
    initialize: function() {
        for(let i = 0; i < 9; i++) {
            this.boardData[i] = 'N';
        }
        this.setup_containers();
    },
    read_frame: function(typeOfFrame, frame) {
        if(typeOfFrame === 'n') {
            for(let i = 0; i < 9; i++) {
                this.boardData[i] = read_char_from_data(frame);
            }
        }
        const boardState = read_char_from_data(frame);
        if(boardState === 'R') {
            for(let i = 0; i < 9; i++) {
                this.boardData[i] = 'N';
            }
            this.setup_containers();
        }
        else if(boardState === 'V') {
            for(let i = 0; i < 9; i++) {
                if(this.boardData[i] !== 'X' && this.boardData[i] !== 'O') {
                    this.boardData[i] = 'N';
                }
            }
            this.setup_containers();
            this.select_mode();
        }
        else if(boardState === 'T') {
            let numberOfEmptySpaces = 0;
            let voteData = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            for(let i = 0; i < 9; i++) {
                if(this.boardData[i] !== 'X' && this.boardData[i] !== 'O') {
                    this.boardData[i] = 'N';
                }
                voteData[i] = read_uint16_from_data(frame);
            }
            const winningSlot = read_uint8_from_data(frame);
            if(this.boardData[winningSlot] === 'N') {
                this.boardData[winningSlot] = this.is_x_playing() ? 'X' : 'O';
            }
            this.setup_containers();
            this.votes_view_mode(voteData);
        }
    },
    setup_containers: function() {
        this.boardContainer.removeChildren();
        this.announcementContainer.removeChildren();
        const boardAnimation = new PIXI.AnimatedSprite(textures.tictactoeboard);
        boardAnimation.x = ARENA_SIZE_X / 2 - this.BOARD_SIZE / 2;
        boardAnimation.y = this.BOARD_Y_POS;
        boardAnimation.width = this.BOARD_SIZE;
        boardAnimation.height = this.BOARD_SIZE;
        boardAnimation.play();
        this.boardContainer.addChild(boardAnimation);
        this.slotContainers = [];
        for(let i = 0; i < 9; i++) {
            this.slotContainers[i] = new PIXI.Container();
            this.slotContainers[i].x = (ARENA_SIZE_X / 2 - this.BOARD_SIZE / 2) + ((i % 3) * (this.BOARD_SIZE / 3));
            this.slotContainers[i].y = this.BOARD_Y_POS + Math.floor(i / 3) * (this.BOARD_SIZE / 3);
            let animSprite = null;
            if(this.boardData[i] === 'X') {
                animSprite = new PIXI.AnimatedSprite(textures.X);
                animSprite.width = 0.8 * (this.BOARD_SIZE / 3);
                animSprite.height = 0.8 * (this.BOARD_SIZE / 3);
            }
            else if(this.boardData[i] === 'O') {
                animSprite = new PIXI.AnimatedSprite(textures.O);
                animSprite.width = 0.8 * (this.BOARD_SIZE / 3);
                animSprite.height = 0.8 * (this.BOARD_SIZE / 3);
            }
            else if(this.boardData[i] === 'VX') {
                animSprite = new PIXI.AnimatedSprite(textures.X);
                animSprite.width = 0.9 * (this.BOARD_SIZE / 3);
                animSprite.height = 0.9 * (this.BOARD_SIZE / 3);
                animSprite.alpha = 0.8;
            }
            else if(this.boardData[i] === 'VO') {
                animSprite = new PIXI.AnimatedSprite(textures.O);
                animSprite.width = 0.9 * (this.BOARD_SIZE / 3);
                animSprite.height = 0.9 * (this.BOARD_SIZE / 3);
                animSprite.alpha = 0.8;
            }
            if(animSprite !== null) {
                animSprite.anchor.set(0.5);
                animSprite.x = this.BOARD_SIZE / 6;
                animSprite.y = this.BOARD_SIZE / 6;
                animSprite.play();
                this.slotContainers[i].addChild(animSprite);
            }
            this.boardContainer.addChild(this.slotContainers[i]);
        }
        let winnerX = this.check_if_winner('X');
        let winnerO = this.check_if_winner('O');
        this.draw_winner_line(winnerX, 0xff1e00);
        this.draw_winner_line(winnerO, 0x009dff);

        if(winnerX.isWinner) {
            this.draw_announcement("WIPE OUT", "#ff1e00");
        }
        else if(winnerO.isWinner) {
            this.draw_announcement("CROWBARS", "#009dff", 440);
        }
        else {
            let boardFull = true;
            for(let i = 0; i < 9; i++) {
                if(this.boardData[i] !== 'X' && this.boardData[i] !== 'O') {
                    boardFull = false;
                    break;
                }
            }
            if(boardFull) {
                this.draw_announcement("...", "#bbbbbb");
            }
        }
    },
    draw_announcement: function(announcementMessage, announcementFill, announcementSize = 500) {
        const announcementStyle = new PIXI.TextStyle({
            fontFamily: 'Indie Flower',
            fontSize: announcementSize,
            fontWeight: 'bold',
            align: 'center',
            fill: announcementFill
        });
        const announcementText = new PIXI.Text(announcementMessage, announcementStyle);
        announcementText.anchor.set(0.5, 0.0);
        announcementText.x = ARENA_SIZE_X / 2;
        announcementText.y = 20;
        this.announcementContainer.addChild(announcementText);
    },
    draw_winner_line: function(winnerData, lineTint) {
        if(winnerData.isWinner) {
            const animSprite = new PIXI.AnimatedSprite(textures.winline);
            animSprite.anchor.set(0.5);
            animSprite.width = this.BOARD_SIZE;
            animSprite.height = this.BOARD_SIZE;
            animSprite.x = (ARENA_SIZE_X / 2) + winnerData.drawLinePos[0] * (this.BOARD_SIZE / 3);
            animSprite.y = this.BOARD_Y_POS + (this.BOARD_SIZE / 2) + winnerData.drawLinePos[1] * (this.BOARD_SIZE / 3);
            animSprite.angle = winnerData.drawLineAngle;
            animSprite.tint = lineTint;
            animSprite.play();
            this.boardContainer.addChild(animSprite);
        }
    },
    is_x_playing: function() {
        const numOfX = this.boardData.filter((a) => { return a === 'X'; }).length;
        const numOfO = this.boardData.filter((a) => { return a === 'O'; }).length;
        return (numOfX === numOfO);
    },
    votes_view_mode: function(voteData) {
        const textStyle = new PIXI.TextStyle({
            fontFamily: 'Indie Flower',
            fontSize: 400,
            fontWeight: 'bold',
            align: 'center'
        });
        for(let i = 0; i < 9; i++) {
            if(voteData[i] !== 0) {
                if(this.slotContainers[i].children.length > 0) {
                    this.slotContainers[i].getChildAt(0).alpha = 0.5;
                }
                if(voteData[i] >= 10) {
                    textStyle.fontSize = 330;
                }
                else if(voteData[i] >= 100) {
                    textStyle.fontSize = 270;
                }
                const pointText = new PIXI.Text(voteData[i], textStyle);
                pointText.anchor.set(0.5);
                pointText.x = this.BOARD_SIZE / 6;
                pointText.y = this.BOARD_SIZE / 6;
                this.slotContainers[i].addChild(pointText);
            }
        }
    },
    select_mode: function() {
        const isXPlaying = this.is_x_playing();
        this.draw_announcement("VOTE", isXPlaying ? "#ff1e00" : "#009dff");
        for(let i = 0; i < 9; i++) {
            if(this.boardData[i] === 'N') {
                const animSprite = new PIXI.AnimatedSprite(textures.selectsquare);
                animSprite.width = (this.BOARD_SIZE / 3);
                animSprite.height = (this.BOARD_SIZE / 3);
                animSprite.alpha = 0.5;
                animSprite.anchor.set(0.5);
                animSprite.x = this.BOARD_SIZE / 6;
                animSprite.y = this.BOARD_SIZE / 6;
                animSprite.interactive = true;
                animSprite.buttonMode = true;
                animSprite.tint = isXPlaying ? 0xff1e00 : 0x009dff;
                animSprite.play();
                this.slotContainers[i].addChild(animSprite);
                animSprite.on('pointerout', function() {
                    animSprite.textures = textures.selectsquare;
                    animSprite.tint = isXPlaying ? 0xff1e00 : 0x009dff;
                    animSprite.gotoAndPlay(0);
                });
                animSprite.on('pointerover', function() {
                    animSprite.textures = isXPlaying ? textures.X : textures.O;
                    animSprite.tint = 0xffffff;
                    animSprite.gotoAndPlay(0);
                });
                animSprite.on('pointerdown', function() {
                    const bufferToSend = new ArrayBuffer(1 + 1);
                    new DataView(bufferToSend).setUint8(0, ('t').codePointAt(0));
                    new DataView(bufferToSend).setUint8(1, i);
                    socket.send(bufferToSend);
                    ticTacToe.boardData[i] = isXPlaying ? 'VX' : 'VO';
                    ticTacToe.setup_containers();
                });
            }
        }
    },
    check_if_winner: function(charToCheck) {
        let winnerData = {
            isWinner: false,
            drawLinePos: [0, 0],
            drawLineAngle: 0
        };
        if(this.boardData[0] === charToCheck && this.boardData[4] === charToCheck && this.boardData[8] === charToCheck) {
            winnerData.isWinner = true;
            winnerData.drawLineAngle = 45;
        }
        else if(this.boardData[1] === charToCheck && this.boardData[4] === charToCheck && this.boardData[7] === charToCheck) {
            winnerData.isWinner = true;
            winnerData.drawLineAngle = 90;
        }
        else if(this.boardData[2] === charToCheck && this.boardData[4] === charToCheck && this.boardData[6] === charToCheck) {
            winnerData.isWinner = true;
            winnerData.drawLineAngle = 135;
        }
        else if(this.boardData[3] === charToCheck && this.boardData[4] === charToCheck && this.boardData[5] === charToCheck) {
            winnerData.isWinner = true;
        }
        else if(this.boardData[0] === charToCheck && this.boardData[1] === charToCheck && this.boardData[2] === charToCheck) {
            winnerData.isWinner = true;
            winnerData.drawLinePos[1] = -1;
        }
        else if(this.boardData[6] === charToCheck && this.boardData[7] === charToCheck && this.boardData[8] === charToCheck) {
            winnerData.isWinner = true;
            winnerData.drawLinePos[1] = 1;
        }
        else if(this.boardData[0] === charToCheck && this.boardData[3] === charToCheck && this.boardData[6] === charToCheck) {
            winnerData.isWinner = true;
            winnerData.drawLinePos[0] = -1;
            winnerData.drawLineAngle = 90;
        }
        else if(this.boardData[2] === charToCheck && this.boardData[5] === charToCheck && this.boardData[8] === charToCheck) {
            winnerData.isWinner = true;
            winnerData.drawLinePos[0] = 1;
            winnerData.drawLineAngle = 90;
        }
        return winnerData;
    }
};

let scoreboard = {
    lastAppHeight: 0,
    lastAppWidth: 0,
    data: [],
    container: new PIXI.Container(),
    read_frame: function(frame) {
        this.data = [];
        const numberOfEntries = read_uint16_from_data(frame);
        for(let i = 0; i < numberOfEntries; i++) {
            const newObj = {};
            newObj.name = read_string_from_data(frame);
            newObj.score = read_uint16_from_data(frame);
            this.data.push(newObj);
        }
    },
    update_display: function(forceUpdate) {
        if(forceUpdate || this.lastAppHeight !== app.screen.height || this.lastAppWidth !== app.screen.width) {
            this.container.removeChildren();
            const SCOREBOARD_FONTSIZE_FACTOR = 0.03;
            const SCOREBOARD_BACK_LEFT_PADDING = 5;
            const SCOREBOARD_BACK_RIGHT_PADDING = 5;
            const SCOREBOARD_BACK_BOTTOM_PADDING = 5;
            const textStyle = new PIXI.TextStyle({
                fontFamily: 'Indie Flower',
                fontSize: app.screen.height * SCOREBOARD_FONTSIZE_FACTOR,
                fontWeight: 'bold',
                align: 'right',
                fill: '#ffffff'
            });
            const titleText = new PIXI.Text("Most Consecutive Bonks", textStyle);
            titleText.anchor.set(1, 0);
            titleText.x = app.screen.width - SCOREBOARD_BACK_RIGHT_PADDING;
            titleText.y = 0;
            let scoreboardNamesString = "";
            let scoreboardScoresString = "";
            for(let i = 0; i < this.data.length; i++) {
                scoreboardNamesString += this.data[i].name + '\n';
                scoreboardScoresString += this.data[i].score + '\n';
            }
            const nameTextStyle = new PIXI.TextStyle({
                fontFamily: 'Indie Flower',
                fontSize: app.screen.height * SCOREBOARD_FONTSIZE_FACTOR,
                fontWeight: 'normal',
                align: 'left',
                fill: '#ffffff'
            });
            const nameText = new PIXI.Text(scoreboardNamesString, nameTextStyle);
            nameText.anchor.set(0, 0);
            nameText.x = app.screen.width - titleText.width - SCOREBOARD_BACK_RIGHT_PADDING;
            nameText.y = titleText.height;
            const scoreTextStyle = new PIXI.TextStyle({
                fontFamily: 'Indie Flower',
                fontSize: app.screen.height * SCOREBOARD_FONTSIZE_FACTOR,
                fontWeight: 'normal',
                align: 'right',
                fill: '#ffffff'
            });
            const scoreText = new PIXI.Text(scoreboardScoresString, scoreTextStyle);
            scoreText.anchor.set(1, 0);
            scoreText.x = app.screen.width - SCOREBOARD_BACK_RIGHT_PADDING;
            scoreText.y = titleText.height;
            const scoreboardBackground = new PIXI.Graphics();
            scoreboardBackground.beginFill(0x000000);
            scoreboardBackground.alpha = 0.6;
            scoreboardBackground.drawRect(app.screen.width - titleText.width - SCOREBOARD_BACK_LEFT_PADDING - SCOREBOARD_BACK_RIGHT_PADDING, 0, titleText.width + SCOREBOARD_BACK_LEFT_PADDING + SCOREBOARD_BACK_RIGHT_PADDING, scoreText.height + SCOREBOARD_BACK_BOTTOM_PADDING);
            this.container.addChild(scoreboardBackground);
            this.container.addChild(titleText);
            this.container.addChild(scoreText);
            this.container.addChild(nameText);
            this.lastAppHeight = app.screen.height;
            this.lastAppWidth = app.screen.width;
        }
    }
};

let queueboard = {
    lastAppHeight: 0,
    lastAppWidth: 0,
    data: [],
    container: new PIXI.Container(),
    read_frame: function(frame) {
        this.data = [];
        const numberOfEntries = read_uint8_from_data(frame);
        for(let i = 0; i < numberOfEntries; i++) {
            this.data.push(read_string_from_data(frame));
        }
    },
    update_display: function(forceUpdate) {
        if(forceUpdate || this.lastAppHeight !== app.screen.height || this.lastAppWidth !== app.screen.width) {
            this.container.removeChildren();
            let queueboardNextSize = app.screen.height * 0.05;
            let textYLocation = 5;
            let textOpacity = 1;
            for(let i = 0; i < this.data.length; i++) {
                const isOwnName = mainMenu.textboxName === this.data[i];
                const textStyle = new PIXI.TextStyle({
                    fontFamily: 'Indie Flower',
                    fontSize: queueboardNextSize,
                    fontWeight: isOwnName ? 'bold' : 'normal',
                    align: 'left',
                    fill: isOwnName ? '#378C46' : '#000000'
                });
                const nameText = new PIXI.Text(this.data[i], textStyle);
                nameText.x = 5;
                nameText.y = textYLocation;
                nameText.opacity = textOpacity;
                textOpacity *= 0.7;
                queueboardNextSize *= 0.9;
                textYLocation += nameText.height;
                this.container.addChild(nameText);
            }
            this.lastAppHeight = app.screen.height;
            this.lastAppWidth = app.screen.width;
        }
    }
};

let touchControls = {
    lastAppHeight: 0,
    lastAppWidth: 0,
    draggingData: null,
    touchEnabled: false,
    displayPopup: true,
    data: [],
    container: new PIXI.Container(),
    initialize: function(isTouch) {
        this.touchEnabled = isTouch;
        this.update_display(true);
    },
    update_display: function(forceUpdate) {
        if(forceUpdate || this.lastAppHeight !== app.screen.height || this.lastAppWidth !== app.screen.width) {
            this.container.removeChildren();
            if(this.touchEnabled) {
                this.draggingData = null;
                let circleDistanceFromBorder = 10;
                let moveCircleRadius = app.screen.height * 0.2;
                let smallMoveCircleRadius = moveCircleRadius * 0.3;
                let circleCenter = moveCircleRadius + circleDistanceFromBorder;
                const moveCircleContainer = new PIXI.Container();
                moveCircleContainer.x = circleCenter;
                moveCircleContainer.y = app.screen.height - circleCenter;
                const moveCircleBack = new PIXI.Graphics();
                moveCircleBack.x = 0;
                moveCircleBack.y = 0;
                moveCircleBack.beginFill(0x68587F, 0.3);
                moveCircleBack.drawCircle(0, 0, moveCircleRadius);
                const moveCircleFront = new PIXI.Graphics();
                moveCircleFront.x = 0;
                moveCircleFront.y = 0;
                moveCircleFront.alpha = 1.0;
                moveCircleFront.beginFill(0xC78753);
                moveCircleFront.drawCircle(0, 0, smallMoveCircleRadius);
                let update_touch_position = function() {
                    if(touchControls.draggingData !== null) {
                        const newPos = touchControls.draggingData.getLocalPosition(moveCircleContainer);
                        const distanceFromCenter = Math.sqrt(newPos.x * newPos.x + newPos.y * newPos.y);
                        const moveCircleLimit = moveCircleRadius - smallMoveCircleRadius;
                        if(distanceFromCenter > moveCircleLimit) {
                            moveCircleFront.x = newPos.x * moveCircleLimit / distanceFromCenter;
                            moveCircleFront.y = newPos.y * moveCircleLimit / distanceFromCenter;
                        }
                        else {
                            moveCircleFront.x = newPos.x;
                            moveCircleFront.y = newPos.y;
                        }
                    }
                    else {
                        moveCircleFront.x = 0;
                        moveCircleFront.y = 0;
                    }
                    if(moveCircleFront.x < -moveCircleRadius * 0.3) {
                        keysPressed.right.touchPressed = false;
                        keysPressed.left.touchPressed = true;
                    }
                    else if(moveCircleFront.x > moveCircleRadius * 0.3) {
                        keysPressed.right.touchPressed = true;
                        keysPressed.left.touchPressed = false;
                    }
                    else {
                        keysPressed.right.touchPressed = false;
                        keysPressed.left.touchPressed = false;
                    }
                    if(moveCircleFront.y < -moveCircleRadius * 0.3) {
                        keysPressed.down.touchPressed = false;
                        keysPressed.up.touchPressed = true;
                    }
                    else if(moveCircleFront.y > moveCircleRadius * 0.3) {
                        keysPressed.down.touchPressed = true;
                        keysPressed.up.touchPressed = false;
                    }
                    else {
                        keysPressed.down.touchPressed = false;
                        keysPressed.up.touchPressed = false;
                    }
                };
                moveCircleBack.interactive = true;
                moveCircleBack.buttonMode = true;
                moveCircleBack.on('pointerdown', (event) => {
                    touchControls.draggingData = event.data;
                    update_touch_position();
                });
                moveCircleBack.on('pointerup', () => {
                    touchControls.draggingData = null;
                    update_touch_position();
                });
                moveCircleBack.on('pointerupoutside', () => {
                    touchControls.draggingData = null;
                    update_touch_position();
                });
                moveCircleBack.on('pointermove', () => {
                    update_touch_position();
                });
                moveCircleContainer.addChild(moveCircleBack);
                moveCircleContainer.addChild(moveCircleFront);
                this.container.addChild(moveCircleContainer);
                
                const SHOOT_BUTTON_DISTANCE_FROM_BORDER = 10;
                const SHOOT_BUTTON_RADIUS = app.screen.height * 0.1;
                const shootButtonContainer = new PIXI.Container();
                shootButtonContainer.x = app.screen.width - SHOOT_BUTTON_DISTANCE_FROM_BORDER - SHOOT_BUTTON_RADIUS;
                shootButtonContainer.y = app.screen.height - SHOOT_BUTTON_DISTANCE_FROM_BORDER - SHOOT_BUTTON_RADIUS;
                shootButtonContainer.alpha = 0.5;
                const buttonCircle = new PIXI.Graphics();
                buttonCircle.x = 0;
                buttonCircle.y = 0;
                buttonCircle.beginFill(0x68587F);
                buttonCircle.drawCircle(0, 0, SHOOT_BUTTON_RADIUS);
                buttonCircle.beginFill(0xC78753);
                buttonCircle.drawCircle(0, 0, SHOOT_BUTTON_RADIUS - 10);
                buttonCircle.interactive = true;
                buttonCircle.buttonMode = true;
                buttonCircle.on('pointerdown', (event) => {
                    keysPressed.shoot.touchPressed = true;
                    shootButtonContainer.alpha = 0.8;
                });
                buttonCircle.on('pointerup', (event) => {
                    keysPressed.shoot.touchPressed = false;
                    shootButtonContainer.alpha = 0.5;
                });
                buttonCircle.on('pointerupoutside', () => {
                    keysPressed.shoot.touchPressed = false;
                    shootButtonContainer.alpha = 0.5;
                });
                shootButtonContainer.addChild(buttonCircle);
                const shootSprite = new PIXI.Sprite(textures.shooticon[0].texture);
                shootSprite.anchor.set(0.5);
                shootSprite.x = 0;
                shootSprite.y = 0;
                shootSprite.width = SHOOT_BUTTON_RADIUS * 1.75;
                shootSprite.height = SHOOT_BUTTON_RADIUS * 1.75;
                shootButtonContainer.addChild(shootSprite);
                this.container.addChild(shootButtonContainer);
            }
            else if(this.displayPopup) {
                const popupDistanceFromRight = app.screen.height * 0.15;
                
                const textStyle = new PIXI.TextStyle({
                    fontFamily: 'Indie Flower',
                    fontSize: app.screen.height * 0.05,
                    fontWeight: 'normal',
                    align: 'center',
                    fill: '#FFFFFF'
                });
                const spaceText = new PIXI.Text("to shoot", textStyle);
                spaceText.anchor.set(0.5, 1.0);
                spaceText.x = app.screen.width - popupDistanceFromRight;
                spaceText.y = app.screen.height;
                const spaceSprite = new PIXI.Sprite(textures.space[0].texture);
                spaceSprite.anchor.set(0.5, 1.0);
                spaceSprite.x = app.screen.width - popupDistanceFromRight;
                spaceSprite.y = spaceText.y - spaceText.height;
                spaceSprite.width = popupDistanceFromRight * 1.75;
                spaceSprite.height = spaceSprite.width * 2 / 8;
                const wasdText = new PIXI.Text("to move", textStyle);
                wasdText.anchor.set(0.5, 1.0);
                wasdText.x = app.screen.width - popupDistanceFromRight;
                wasdText.y = spaceSprite.y - spaceSprite.height;
                const wasdSprite = new PIXI.Sprite(textures.wasd[0].texture);
                wasdSprite.anchor.set(0.5, 1.0);
                wasdSprite.x = app.screen.width - popupDistanceFromRight;
                wasdSprite.y = wasdText.y - wasdText.height;
                wasdSprite.width = popupDistanceFromRight;
                wasdSprite.height = wasdSprite.width * 3 / 4;
                closeSprite = new PIXI.Sprite(textures.close[0].texture);
                closeSprite.anchor.set(0.5);
                closeSprite.x = app.screen.width - popupDistanceFromRight * 2.0;
                closeSprite.y = wasdSprite.y - wasdSprite.height;
                closeSprite.width = app.screen.height * 0.05;
                closeSprite.height = closeSprite.width;
                closeSprite.interactive = true;
                closeSprite.buttonMode = true;
                closeSprite.on('pointerdown', (event) => {
                    touchControls.displayPopup = false;
                    touchControls.container.removeChildren();
                });
                closeSprite.on('pointerover', (event) => {
                    closeSprite.width = app.screen.height * 0.07;
                    closeSprite.height = closeSprite.width;
                });
                closeSprite.on('pointerout', () => {
                    closeSprite.width = app.screen.height * 0.05;
                    closeSprite.height = closeSprite.width;
                });
                const popupBackground = new PIXI.Graphics();
                popupBackground.beginFill(0x000000);
                popupBackground.alpha = 0.6;
                popupBackground.drawRect(closeSprite.x, closeSprite.y, app.screen.width - closeSprite.x, app.screen.height - closeSprite.y);
                this.container.addChild(popupBackground);
                this.container.addChild(spaceText);
                this.container.addChild(spaceSprite);
                this.container.addChild(wasdText);
                this.container.addChild(wasdSprite);
                this.container.addChild(closeSprite);
            }
            this.lastAppHeight = app.screen.height;
            this.lastAppWidth = app.screen.width;
        }
    }
};

let resizer = {
    lastAppHeight: 0,
    lastAppWidth: 0,
    update_display: function(forceUpdate) {
        if(forceUpdate || this.lastAppHeight !== window.innerHeight || this.lastAppWidth !== window.innerWidth) {
            app.resizeTo = window;
            app.resize();
            this.lastAppHeight = window.innerHeight;
            this.lastAppWidth = window.innerWidth;
            window.scrollTo(0, 0);
        }
    }
};

let tilingBackground = null;
let inabackSprite = null;
let inaeyesSprite = null;
let worldContainer = null;
let inafrontSprite = null;

let inputsToSend = {}

function clear_inputs_to_send() {
    inputsToSend = {
        leftMillisecondsPressed: 0.0,
        rightMillisecondsPressed: 0.0,
        downPressedAt: 0.0,
        upPressedAt: 0.0,
        shootAngle: SHOOT_ANGLE_MINIMUM,
        shootPressed: false
    };
}

let keysPressed = {};
let gameObjects = [];
let socket = null;
let timeSinceLastPacketSent = 0.0;
let gameObjToControlID = null;

const ANGLE_CHANGE_PER_SECOND = 40.0;

function string_to_buffer(strToConvert) {
    const encoder = new TextEncoder();
    const encoderView = encoder.encode(strToConvert);
    const strToConvertLength = encoderView.length;
    const buffer = new ArrayBuffer(1 + strToConvertLength);
    
    // Encode string length
    const bufferDataView = new DataView(buffer);
    bufferDataView.setUint8(0, strToConvertLength);
    
    // Encode string data
    const uint8View = new Uint8Array(buffer, 1);
    uint8View.set(encoderView);
    
    return buffer;
}

function read_char_from_data(frame) {
    let uint8 = new DataView(frame.data).getUint8(frame.offset);
    frame.offset += 1;
    return String.fromCodePoint(uint8);
}

function read_uint8_from_data(frame) {
    let uint8 = new DataView(frame.data).getUint8(frame.offset);
    frame.offset += 1;
    return uint8;
}

function read_uint16_from_data(frame) {
    let uint16 = new DataView(frame.data).getUint16(frame.offset);
    frame.offset += 2;
    return uint16;
}

function read_int16_from_data(frame) {
    let int16 = new DataView(frame.data).getInt16(frame.offset);
    frame.offset += 2;
    return int16;
}

function read_string_from_data(frame) {
    let strLength = read_uint8_from_data(frame);
    let strUint8Array = new Uint8Array(frame.data, frame.offset, strLength);
    frame.offset += strLength;
    return new TextDecoder().decode(strUint8Array);
}

function read_tako_new(frame, worldID, objType) {
    let objectToAdd = {};
    
    objectToAdd.set_facing_right = function (isFacingRight) {
        objectToAdd.facingRight = isFacingRight;
        objectToAdd.spriteContainer.scale.x = (isFacingRight ? 1 : -1);
        objectToAdd.spriteContainer.x = (isFacingRight ? 0 : TAKO_SIZE);
    }
    objectToAdd.refresh_animation = function (deltaSeconds) {
        objectToAdd.container.x = objectToAdd.worldXPos;
        objectToAdd.container.y = ARENA_SIZE_Y - objectToAdd.worldYPos - TAKO_SIZE;
        let newTexture = "";
        if(objectToAdd.bonked) {
            newTexture = "takobonked";
        }
        else if(objectToAdd.melting) {
            newTexture = "takomelting";
        }
        else if(objectToAdd.velocity[0] === 0.0) {
            newTexture = objectToAdd.hasCrowbar ? "takocrowbaridle" : "takoidle";
        }
        else {
            newTexture = objectToAdd.hasCrowbar ? "takocrowbarwalk" : "takowalk";
        }
        if(objectToAdd.previousTexture !== newTexture) {
            objectToAdd.spriteContainer.removeChildren();
            const takoSprite = new PIXI.AnimatedSprite(textures[newTexture]);
            takoSprite.width = TAKO_SIZE;
            takoSprite.height = TAKO_SIZE;
            takoSprite.loop = (newTexture !== "takobonked" && newTexture !== "takomelting");
            objectToAdd.spriteContainer.addChild(takoSprite);
            takoSprite.play();
            objectToAdd.previousTexture = newTexture;
        }
        if(objectToAdd.shootWasPressed && objectToAdd.shootingangleSprite === null) {
            objectToAdd.shootingangleSprite = new PIXI.AnimatedSprite(textures.shootingangle);
            objectToAdd.shootingangleSprite.anchor.set(1.0, 0.5);
            objectToAdd.shootingangleSprite.height = -200;
            objectToAdd.shootingangleSprite.width = 600;
            objectToAdd.shootingangleSprite.x = TAKO_SIZE / 2;
            objectToAdd.shootingangleSprite.y = TAKO_SIZE / 2;
            objectToAdd.shootingangleSprite.play();
            objectToAdd.container.addChild(objectToAdd.shootingangleSprite);
        }
        else if(!objectToAdd.shootWasPressed && objectToAdd.shootingangleSprite !== null) {
            objectToAdd.container.removeChild(objectToAdd.shootingangleSprite);
            objectToAdd.shootingangleSprite = null;
        }
        if(objectToAdd.shootingangleSprite !== null) {
            objectToAdd.shootingangleSprite.angle = objectToAdd.facingRight ? (180 - objectToAdd.angleShootAt) : objectToAdd.angleShootAt;
        }
        if(objectToAdd.marked && objectToAdd.meltingMark === null) {
            objectToAdd.meltingMark = new PIXI.AnimatedSprite(textures.X);
            objectToAdd.meltingMark.anchor.set(0.5);
            objectToAdd.meltingMark.x = TAKO_SIZE / 2;
            objectToAdd.meltingMark.y = TAKO_SIZE / 2;
            objectToAdd.meltingMark.width = TAKO_SIZE;
            objectToAdd.meltingMark.height = TAKO_SIZE;
            objectToAdd.meltingMark.play();
            objectToAdd.container.addChild(objectToAdd.meltingMark);
        }
        if(objectToAdd.melting && objectToAdd.meltingMark !== null && objectToAdd.meltingMark.alpha > 0) {
            objectToAdd.meltingMark.width += 1000 * deltaSeconds;
            objectToAdd.meltingMark.height += 1000 * deltaSeconds;
            objectToAdd.meltingMark.alpha -= 3 * deltaSeconds;
            if(objectToAdd.meltingMark.alpha < 0) {
                objectToAdd.meltingMark.alpha = 0;
            }
        }
    }
    objectToAdd.set_velocity_x = function (newVel) {
        objectToAdd.velocity[0] = newVel;
        if(objectToAdd.velocity[0] > 0.0) {
            objectToAdd.set_facing_right(true);
        }
        else if(objectToAdd.velocity[0] < 0.0) {
            objectToAdd.set_facing_right(false);
        }
    }
    objectToAdd.set_state_variables_from_type = function() {
        objectToAdd.hasCrowbar = (objectToAdd.objType === 'w' || objectToAdd.objType === 'x');
        objectToAdd.marked = (objectToAdd.objType === 'x' || objectToAdd.objType === 'u');
        objectToAdd.bonked = (objectToAdd.objType === 'b');
        objectToAdd.melting = (objectToAdd.objType === 'm');
    }
    objectToAdd.objType = objType;
    objectToAdd.worldID = worldID;
    objectToAdd.previousTexture = null;
    objectToAdd.container = new PIXI.Container();
    objectToAdd.spriteContainer = new PIXI.Container();
    objectToAdd.container.addChild(objectToAdd.spriteContainer);
    objectToAdd.worldXPos = read_int16_from_data(frame);
    objectToAdd.worldYPos = read_int16_from_data(frame);
    objectToAdd.container.pivot.x = 0;
    objectToAdd.container.pivot.y = 0;
    objectToAdd.velocity = [0, 0];
    objectToAdd.set_velocity_x(read_int16_from_data(frame));
    objectToAdd.velocity[1] = read_int16_from_data(frame);
    objectToAdd.name = read_string_from_data(frame);
    objectToAdd.set_state_variables_from_type();
    objectToAdd.angleShootAt = SHOOT_ANGLE_MINIMUM;
    objectToAdd.shootWasPressed = false;
    objectToAdd.shootingangleSprite = null;
    objectToAdd.meltingMark = null;
    
    const nameTextStyle = new PIXI.TextStyle({
        fontFamily: 'Indie Flower',
        fontSize: 100,
        fontWeight: 'bold'
    });
    objectToAdd.nameText = new PIXI.Text(objectToAdd.name, nameTextStyle);
    objectToAdd.nameText.anchor.set(0.5, 1.0);
    objectToAdd.nameText.x = TAKO_SIZE / 2;
    objectToAdd.nameText.y = 0;
    objectToAdd.container.addChild(objectToAdd.nameText);
    objectToAdd.changedColorIfOwner = false;
    
    gameObjects.push(objectToAdd);
    worldContainer.addChild(objectToAdd.container);
    
    objectToAdd.main_loop_update = function(deltaSeconds) {
        if(!objectToAdd.bonked && !objectToAdd.melting) {
            objectToAdd.velocity[1] -= GRAVITY * deltaSeconds;
            if(objectToAdd.velocity[1] <= TAKO_MAX_DOWNWARD_Y_VELOCITY) {
                objectToAdd.velocity[1] = TAKO_MAX_DOWNWARD_Y_VELOCITY;
            }
            if(objectToAdd.worldID === gameObjToControlID) {
                if(!objectToAdd.changedColorIfOwner) {
                    objectToAdd.container.removeChild(objectToAdd.nameText);
                    const nameTextStyle = new PIXI.TextStyle({
                        fontFamily: 'Indie Flower',
                        fontSize: 150,
                        fontWeight: 'bold',
                        fill: "#378C46"
                    });
                    objectToAdd.nameText = new PIXI.Text(objectToAdd.name, nameTextStyle);
                    objectToAdd.nameText.anchor.set(0.5, 1.0);
                    objectToAdd.nameText.x = TAKO_SIZE / 2;
                    objectToAdd.nameText.y = 0;
                    objectToAdd.container.addChild(objectToAdd.nameText);
                    objectToAdd.changedColorIfOwner = true;
                }
                if(keysPressed.left.pressed && !keysPressed.right.pressed) {
                    objectToAdd.set_velocity_x(-TAKO_SPEED);
                }
                else if(!keysPressed.left.pressed && keysPressed.right.pressed) {
                    objectToAdd.set_velocity_x(TAKO_SPEED);
                }
                else {
                    objectToAdd.set_velocity_x(0.0);
                }
                if(keysPressed.up.pressed && !keysPressed.down.pressed && objectToAdd.worldYPos === 0.0) {
                    objectToAdd.velocity[1] = TAKO_JUMP_VELOCITY;
                }
                else if(keysPressed.down.pressed && objectToAdd.worldYPos !== 0.0) {
                    objectToAdd.velocity[1] = TAKO_MAX_DOWNWARD_Y_VELOCITY;
                }
                if(objectToAdd.hasCrowbar) {
                    if(keysPressed.shoot.pressed && !inputsToSend.shootPressed) {
                        objectToAdd.angleShootAt += ANGLE_CHANGE_PER_SECOND * deltaSeconds;
                        if(objectToAdd.angleShootAt > SHOOT_ANGLE_MAXIMUM) {
                            objectToAdd.angleShootAt = SHOOT_ANGLE_MAXIMUM;
                        }
                        objectToAdd.shootWasPressed = true;
                    }
                    if(objectToAdd.shootWasPressed && !keysPressed.shoot.pressed) {
                        inputsToSend.shootPressed = true;
                        inputsToSend.shootAngle = objectToAdd.facingRight ? objectToAdd.angleShootAt : -objectToAdd.angleShootAt;
                        objectToAdd.shootWasPressed = false;
                        objectToAdd.angleShootAt = SHOOT_ANGLE_MINIMUM;
                    }
                }
                else {
                    objectToAdd.shootWasPressed = false;
                    objectToAdd.angleShootAt = SHOOT_ANGLE_MINIMUM;
                }
            }
            objectToAdd.worldXPos += objectToAdd.velocity[0] * deltaSeconds;
            objectToAdd.worldYPos += objectToAdd.velocity[1] * deltaSeconds;
            if(objectToAdd.worldXPos <= 0.0) {
                objectToAdd.worldXPos = 0.0;
            }
            if(objectToAdd.worldXPos >= ARENA_SIZE_X - TAKO_SIZE) {
                objectToAdd.worldXPos = ARENA_SIZE_X - TAKO_SIZE;
            }
            if(objectToAdd.worldYPos <= 0.0) {
                objectToAdd.worldYPos = 0.0;
                objectToAdd.velocity[1] = 0.0;
            }
        }
        objectToAdd.refresh_animation(deltaSeconds);
    }
    objectToAdd.call_on_destruction = function(worldIDToRemove) {
        worldContainer.removeChild(objectToAdd.container);
        if(gameObjToControlID === worldIDToRemove) {
            gameObjToControlID = null;
            shootWasPressed = false;
            angleShootAt = 0;
        }
    };
    objectToAdd.read_update_frame = function(frame) {
        const takoState = read_char_from_data(frame);
        objectToAdd.objType = takoState;
        let posX = read_int16_from_data(frame);
        let posY = read_int16_from_data(frame);
        if(objectToAdd.worldID !== gameObjToControlID || Math.abs(posX - objectToAdd.worldXPos) > 40 || Math.abs(posY - objectToAdd.worldYPos) > 40 || (objectToAdd.velocity[0] === 0 && objectToAdd.velocity[1] === 0)) {
            objectToAdd.worldXPos = posX;
            objectToAdd.worldYPos = posY;
        }
        objectToAdd.set_velocity_x(read_int16_from_data(frame));
        objectToAdd.velocity[1] = read_int16_from_data(frame);
        objectToAdd.set_state_variables_from_type();
    };
    
    objectToAdd.set_facing_right(objectToAdd.velocity[0] >= 0);
}

function read_crowbar_new(frame, worldID, objType) {
    const CROWBAR_SPRITE_SIZE = 240.0;
    
    let objectToAdd = {};
    objectToAdd.previousTexture = "crowbar";
    objectToAdd.objType = objType;
    objectToAdd.worldID = worldID;
    objectToAdd.refresh_animation = function() {
        objectToAdd.container.x = objectToAdd.worldXPos;
        objectToAdd.container.y = ARENA_SIZE_Y - objectToAdd.worldYPos - CROWBAR_SPRITE_SIZE;
    };
    objectToAdd.container = new PIXI.Container();
    const crowbarSprite = new PIXI.AnimatedSprite(textures.crowbar);
    crowbarSprite.width = CROWBAR_SPRITE_SIZE;
    crowbarSprite.height = CROWBAR_SPRITE_SIZE;
    crowbarSprite.x = (CROWBAR_SIZE - CROWBAR_SPRITE_SIZE) / 2;
    crowbarSprite.y = (CROWBAR_SIZE - CROWBAR_SPRITE_SIZE) / 2;
    objectToAdd.container.addChild(crowbarSprite);
    objectToAdd.worldXPos = read_int16_from_data(frame);
    objectToAdd.worldYPos = read_int16_from_data(frame);
    objectToAdd.velocity = [0, 0];
    objectToAdd.velocity[0] = read_int16_from_data(frame);
    objectToAdd.velocity[1] = read_int16_from_data(frame);
    if(objType === 'd') {
        objectToAdd.markOnSummon = new PIXI.AnimatedSprite(textures.O);
        objectToAdd.markOnSummon.anchor.set(0.5);
        objectToAdd.markOnSummon.x = CROWBAR_SIZE / 2;
        objectToAdd.markOnSummon.y = CROWBAR_SIZE / 2;
        objectToAdd.markOnSummon.width = CROWBAR_SPRITE_SIZE;
        objectToAdd.markOnSummon.height = CROWBAR_SPRITE_SIZE;
        objectToAdd.markOnSummon.play();
        objectToAdd.container.addChild(objectToAdd.markOnSummon);
    }
    else {
        objectToAdd.markOnSummon = null;
    }
    gameObjects.push(objectToAdd);
    worldContainer.addChild(objectToAdd.container);
    crowbarSprite.play();
    objectToAdd.refresh_animation();
    
    
    objectToAdd.main_loop_update = function(deltaSeconds) {
        objectToAdd.velocity[1] -= GRAVITY * deltaSeconds;
        objectToAdd.worldXPos += objectToAdd.velocity[0] * deltaSeconds;
        objectToAdd.worldYPos += objectToAdd.velocity[1] * deltaSeconds;
        if(objectToAdd.worldXPos <= 0.0) {
            objectToAdd.worldXPos = 0.0;
            objectToAdd.velocity[0] = -objectToAdd.velocity[0];
        }
        if(objectToAdd.worldXPos >= ARENA_SIZE_X - CROWBAR_SIZE) {
            objectToAdd.worldXPos = ARENA_SIZE_X - CROWBAR_SIZE;
            objectToAdd.velocity[0] = -objectToAdd.velocity[0];
        }
        if(objectToAdd.worldYPos <= 0.0) {
            objectToAdd.worldYPos = 0.0;
            objectToAdd.velocity[1] = 0.0;
            objectToAdd.velocity[0] = 0.0;
            if(objectToAdd.previousTexture === "crowbar") {
                objectToAdd.container.removeChildren();
                const stationaryCrowbarSprite = new PIXI.AnimatedSprite(textures.crowbarstationary);
                stationaryCrowbarSprite.width = CROWBAR_SPRITE_SIZE;
                stationaryCrowbarSprite.height = CROWBAR_SPRITE_SIZE;
                stationaryCrowbarSprite.x = (CROWBAR_SIZE - CROWBAR_SPRITE_SIZE) / 2;
                stationaryCrowbarSprite.play();
                objectToAdd.container.addChild(stationaryCrowbarSprite);
                objectToAdd.previousTexture = "crowbarstationary";
            }
        }
        if(objectToAdd.markOnSummon !== null && objectToAdd.markOnSummon.alpha > 0) {
            objectToAdd.markOnSummon.width += 1000 * deltaSeconds;
            objectToAdd.markOnSummon.height += 1000 * deltaSeconds;
            objectToAdd.markOnSummon.alpha -= 3 * deltaSeconds;
            if(objectToAdd.markOnSummon.alpha < 0) {
                objectToAdd.markOnSummon.alpha = 0;
            }
        }
        objectToAdd.refresh_animation();
    };
    objectToAdd.call_on_destruction = function(worldIDToRemove) {
        worldContainer.removeChild(objectToAdd.container);
    };
    objectToAdd.read_update_frame = function(frame) {
        objectToAdd.worldXPos = read_int16_from_data(frame);
        objectToAdd.worldYPos = read_int16_from_data(frame);
        objectToAdd.velocity[0] = read_int16_from_data(frame);
        objectToAdd.velocity[1] = read_int16_from_data(frame);
    };
}

function process_frame_data(frameData) {
    let frame = {
        data: frameData,
        offset: 0
    };
    const typeOfFrame = read_char_from_data(frame);
    if(typeOfFrame === 'c') {
        setup_keyboard_listeners();
        app.ticker.remove(mainMenu.loop);
        touchControls.initialize(document.getElementById("enableTouchControls").checked);
        document.getElementById("titleBarContainer").style.display = "none";
        mainMenu.serverListSocket.close();
        mainMenu.socketConnected = false;
        initialize_containers();
        ticTacToe.initialize();
        document.body.style.overflow = "hidden";
        document.body.prepend(app.view);
        document.body.addEventListener("touchmove", function (e) {
            window.scrollTo(0, 0);
        }, {passive:false});
        window.addEventListener("touchmove", function (e) {
            window.scrollTo(0, 0);
        }, {passive:false});
        document.body.addEventListener("scroll", function (e) {
            window.scrollTo(0, 0);
        }, {passive:false});
        window.addEventListener("scroll", function (e) {
            window.scrollTo(0, 0);
        }, {passive:false});
        resizer.update_display(true);
        app.ticker.add(main_loop);
    }
    if(typeOfFrame === 'c' || typeOfFrame === 'u') {
        let worldObjectsRead = [];
        while(frame.offset < frame.data.byteLength) {
            const worldID = read_uint8_from_data(frame);
            const objectToUpdate = get_world_object_from_id(worldID);
            if(objectToUpdate === undefined) {
                const objType = read_char_from_data(frame);
                if(objType === 'b' || objType === 'w' || objType === 't' || objType === 'u' || objType === 'x' || objType === 'm') {
                    read_tako_new(frame, worldID, objType);
                }
                else if(objType === 'c' || objType === 'd') {
                    read_crowbar_new(frame, worldID, objType);
                }
            }
            else {
                objectToUpdate.read_update_frame(frame);
            }
            worldObjectsRead.push(worldID);
        }
        remove_world_unupdated_objects(worldObjectsRead);
    }
    else if(typeOfFrame === 'i') {
        gameObjToControlID = read_uint8_from_data(frame);
    }
    else if(typeOfFrame === 't' || typeOfFrame === 'n') {
        ticTacToe.read_frame(typeOfFrame, frame);
    }
    else if(typeOfFrame === 's') {
        scoreboard.read_frame(frame);
        scoreboard.update_display(true);
    }
    else if(typeOfFrame === 'q') {
        queueboard.read_frame(frame);
        queueboard.update_display(true);
    }
}

function initialize_containers() {
    app.stage.removeChildren();
    tilingBackground = new PIXI.TilingSprite(textures.background[0].texture);
    inabackSprite = new PIXI.AnimatedSprite(textures.inaback);
    inaeyesSprite = new PIXI.AnimatedSprite(textures.inaeyes);
    worldContainer = new PIXI.Container();
    inafrontSprite = new PIXI.AnimatedSprite(textures.inafront);
    
    inabackSprite.play();
    inaeyesSprite.play();
    inafrontSprite.play();

    app.stage.addChild(tilingBackground);
    app.stage.addChild(inabackSprite);
    app.stage.addChild(inaeyesSprite);
    app.stage.addChild(ticTacToe.boardContainer);
    app.stage.addChild(inafrontSprite);
    app.stage.addChild(worldContainer);
    app.stage.addChild(ticTacToe.announcementContainer);
    app.stage.addChild(scoreboard.container);
    app.stage.addChild(queueboard.container);
    app.stage.addChild(touchControls.container);
    
    const tableSprite = new PIXI.AnimatedSprite(textures.table);
    tableSprite.anchor.set(0, 1);
    tableSprite.x = 0;
    tableSprite.y = ARENA_SIZE_Y;
    tableSprite.width = 4000;
    tableSprite.height = 500;
    tableSprite.play();
    worldContainer.addChild(tableSprite);
}

function socket_connect(address) {
    socket = new WebSocket("wss://" + address);
    socket.binaryType = "arraybuffer";
    socket.onopen = function(event) {
        const buffer = string_to_buffer(mainMenu.textboxName);
        socket.send(buffer);
    };
    socket.onmessage = function(event) {
        process_frame_data(event.data);
    };
    socket.onclose = function(event) {
        document.getElementById("serverList").style.display = "flex";
        document.getElementById("connectingMessage").style.display = "none";
        alert("Connection closed: " + event.reason);
    };
    socket.onerror = function(error) {
        document.getElementById("serverList").style.display = "flex";
        document.getElementById("connectingMessage").style.display = "none";
        alert("Connection Error: " + error.message);
    };
}

function get_world_object_from_id(worldObjectID) {
     return gameObjects.find(element => {
         return element.worldID === worldObjectID;
    });
}

function remove_world_unupdated_objects(objIDsToKeep) {
    gameObjects = gameObjects.filter(function (objInArray) {
        for(const objToKeep of objIDsToKeep) {
            if(objToKeep === objInArray.worldID) {
                return true;
            }
        }
        objInArray.call_on_destruction(objInArray.worldID);
        return false;
    });
}

function clear_keys_pressed() {
    keysPressed = {
        up: {keyValue: "w", keyPressed: false, touchPressed: false, pressed: false},
        down: {keyValue: "s", keyPressed: false, touchPressed: false, pressed: false},
        left: {keyValue: "a", keyPressed: false, touchPressed: false, pressed: false},
        right: {keyValue: "d", keyPressed: false, touchPressed: false, pressed: false},
        shoot: {keyValue: " ", keyPressed: false, touchPressed: false, pressed: false}
    };
}

function update_keys_pressed() {
    for(let [key, value] of Object.entries(keysPressed)) {
        value.pressed = value.keyPressed || value.touchPressed;
    }
}

function key_down_event_listener(event) {
    for(let [key, value] of Object.entries(keysPressed)) {
        if(event.key === value.keyValue) {
            value.keyPressed = true;
        }
    }
    event.preventDefault();
}

function key_up_event_listener(event) {
    for(let [key, value] of Object.entries(keysPressed)) {
        if(event.key === value.keyValue) {
            value.keyPressed = false;
        }
    }
    event.preventDefault();
}

function setup_keyboard_listeners() {
    clear_keys_pressed();
    window.addEventListener("keydown", key_down_event_listener, false);
    window.addEventListener("keyup", key_up_event_listener, false);
}

function remove_keyboard_listeners() {
    window.removeEventListener("keydown", key_down_event_listener);
    window.removeEventListener("keyup", key_up_event_listener);
}

function start_game(ipToConnectTo) {
    clear_inputs_to_send();
    socket_connect(ipToConnectTo);
}

function send_input_packet() {
    if(gameObjToControlID !== null) {
        const bufferToSend = new ArrayBuffer(1 + 1 + 2 + 2 + 2 + 2 + 2);
        new DataView(bufferToSend).setUint8(0, ('i').codePointAt(0));
        let inputByte = 0;
        if(keysPressed.left.pressed) {
            inputByte |= 0b1;
        }
        if(keysPressed.right.pressed) {
            inputByte |= 0b10;
        }
        if(inputsToSend.shootPressed) {
            inputByte |= 0b100;
        }
        new DataView(bufferToSend).setUint8(1, inputByte);
        new DataView(bufferToSend).setInt16(2, inputsToSend.shootAngle);
        new DataView(bufferToSend).setUint16(4, inputsToSend.leftMillisecondsPressed);
        new DataView(bufferToSend).setUint16(6, inputsToSend.rightMillisecondsPressed);
        new DataView(bufferToSend).setUint16(8, inputsToSend.upPressedAt);
        new DataView(bufferToSend).setUint16(10, inputsToSend.downPressedAt);
        socket.send(bufferToSend);
    }
    else {
        const bufferToSend = new ArrayBuffer(1);
        new DataView(bufferToSend).setUint8(0, ('r').codePointAt(0));
        socket.send(bufferToSend);
    }
    clear_inputs_to_send();
}

function main_loop(delta) {
    update_keys_pressed();
    resizer.update_display(false);
    touchControls.update_display(false);
    queueboard.update_display(false);
    scoreboard.update_display(false);
    transform_tiling_background(tilingBackground);
    transform_container_to_world_coordinates(inabackSprite);
    transform_container_to_world_coordinates(inaeyesSprite);
    transform_container_to_world_coordinates(inafrontSprite);
    transform_container_to_world_coordinates(worldContainer);
    transform_container_to_world_coordinates(ticTacToe.boardContainer);
    transform_container_to_world_coordinates(ticTacToe.announcementContainer);
    const deltaMilliSeconds = get_delta_milliseconds();
    timeSinceLastPacketSent += deltaMilliSeconds;
    if(inputsToSend.downPressedAt > 0.0) {
        inputsToSend.downPressedAt += deltaMilliSeconds;
    }
    if(inputsToSend.upPressedAt > 0.0) {
        inputsToSend.upPressedAt += deltaMilliSeconds;
    }
    if(keysPressed.left.pressed) {
        inputsToSend.leftMillisecondsPressed += deltaMilliSeconds;
    }
    if(keysPressed.right.pressed) {
        inputsToSend.rightMillisecondsPressed += deltaMilliSeconds;
    }
    if(keysPressed.down.pressed && inputsToSend.downPressedAt === 0.0) {
        inputsToSend.downPressedAt += deltaMilliSeconds;
    }
    if(keysPressed.up.pressed && inputsToSend.upPressedAt === 0.0) {
        inputsToSend.upPressedAt += deltaMilliSeconds;
    }
    const deltaSeconds = deltaMilliSeconds / 1000.0;
    for(let obj of gameObjects) {
        obj.main_loop_update(deltaSeconds);
    }
    if(timeSinceLastPacketSent >= 60.0 && socket.bufferedAmount === 0) {
        send_input_packet();
        timeSinceLastPacketSent = 0.0;
    }
}
