#pragma once
#define ASIO_STANDALONE
#include <websocketpp/config/asio.hpp>
#include <websocketpp/server.hpp>

typedef websocketpp::server<websocketpp::config::asio_tls> server;

typedef websocketpp::lib::shared_ptr<websocketpp::lib::asio::ssl::context> context_ptr;

enum tls_mode {
    MOZILLA_INTERMEDIATE = 1,
    MOZILLA_MODERN = 2
};

class TLSHelper {
public:
    static std::string certificateChainFile;
    static std::string privateKeyFile;
    static std::string tmpDhFile;
    static std::string get_password();
    static context_ptr on_tls_init(tls_mode mode, websocketpp::connection_hdl hdl);
};
