#include "tictactoe.hpp"
#include "constants.hpp"
#include "helpers.hpp"
#include <algorithm>

TicTacToe::TicTacToe(): winningChar('N'), winnerChosen(false), updateReadyToSend(false), state(TicTacToeState::REST), timeInState(TICTACTOE_REST_TIME) { }

void TicTacToe::update(double deltaTime) {
    timeInState += deltaTime;
    if(state == TicTacToeState::REST && timeInState >= TICTACTOE_REST_TIME) {
        winningChar = 'N';
        board.fill('N');
        votes.clear();
        winnerChosen = false;
        updateReadyToSend = true;
        timeInState = 0.0;
        state = TicTacToeState::VOTE;
    }
    else if(state == TicTacToeState::VOTE && timeInState >= TICTACTOE_VOTE_TIME) {
        voteCount.fill(0);
        for(const auto& mapPair : votes) {
            voteCount[mapPair.second] += 1;
        }

        std::vector<unsigned> potentialWinners;
        unsigned potentialWinnersAmount = 0;
        for(unsigned i = 0; i < voteCount.size(); i++) {
            if(board[i] == 'N') {
                if(potentialWinnersAmount == voteCount[i]) {
                    potentialWinners.emplace_back(i);
                }
                else if(potentialWinnersAmount < voteCount[i]) {
                    potentialWinners.clear();
                    potentialWinners.emplace_back(i);
                    potentialWinnersAmount = voteCount[i];
                }
            }
        }
        winningSlot = potentialWinners[RNG::get_random_integer<unsigned>(0, potentialWinners.size() - 1)];

        unsigned numOfX = std::count(board.begin(), board.end(), 'X');
        unsigned numOfO = std::count(board.begin(), board.end(), 'O');
        board[winningSlot] = (numOfX == numOfO) ? 'X' : 'O';
        if(check_if_winner('X')) {
            winningChar = 'X';
        }
        else if(check_if_winner('O')) {
            winningChar = 'O';
        }
        else if(std::count(board.begin(), board.end(), 'N') == 0) {
            winningChar = 'T';
        }
        else {
            winningChar = 'N';
        }
        winnerChosen = winningChar != 'N';

        updateReadyToSend = true;
        timeInState = 0.0;
        state = TicTacToeState::RESULT;
    }
    else if(state == TicTacToeState::RESULT && timeInState >= TICTACTOE_RESULT_TIME) {
        votes.clear();
        updateReadyToSend = true;
        timeInState = 0.0;
        state = !winnerChosen ? TicTacToeState::VOTE : TicTacToeState::REST;
    }
}

bool TicTacToe::get_has_update_and_clear() {
    bool toRet = updateReadyToSend;
    updateReadyToSend = false;
    return toRet;
}

char TicTacToe::get_winning_char_and_clear() {
    char toRet = winningChar;
    winningChar = 'N';
    return toRet;
}

void TicTacToe::write_new(std::vector<uint8_t>& dataBuffer) {
    dataBuffer.emplace_back('n');
    for(unsigned i = 0; i < board.size(); i++) {
        dataBuffer.emplace_back(board[i]);
    }
    write_common(dataBuffer);
}

void TicTacToe::write_update(std::vector<uint8_t>& dataBuffer) {
    dataBuffer.emplace_back('t');
    write_common(dataBuffer);
}

void TicTacToe::write_common(std::vector<uint8_t>& dataBuffer) {
    if(state == TicTacToeState::REST) {
        dataBuffer.emplace_back('R');
    }
    else if(state == TicTacToeState::VOTE) {
        dataBuffer.emplace_back('V');
    }
    else if(state == TicTacToeState::RESULT) {
        dataBuffer.emplace_back('T');
        for(unsigned i = 0; i < voteCount.size(); i++) {
            write_integer_to_buffer<uint16_t>(dataBuffer, static_cast<uint16_t>(voteCount[i]));
        }
        dataBuffer.emplace_back(static_cast<uint8_t>(winningSlot));
    }
}

void TicTacToe::set_vote(const std::shared_ptr<GamePlayer>& player, unsigned voteIndex) {
    unsigned clampedIndex = clamp_value<unsigned>(voteIndex, 0, 8);
    if(player && state == TicTacToeState::VOTE && board[clampedIndex] == 'N') {
        votes[player] = clampedIndex;
    }
}

void TicTacToe::remove_vote(const std::shared_ptr<GamePlayer>& player) {
    if(player) {
        votes.erase(player);
    }
}

bool TicTacToe::check_if_winner(char charToCheck) {
    return (board[0] == charToCheck && board[4] == charToCheck && board[8] == charToCheck) ||
           (board[1] == charToCheck && board[4] == charToCheck && board[7] == charToCheck) ||
           (board[2] == charToCheck && board[4] == charToCheck && board[6] == charToCheck) ||
           (board[3] == charToCheck && board[4] == charToCheck && board[5] == charToCheck) ||
           (board[0] == charToCheck && board[1] == charToCheck && board[2] == charToCheck) ||
           (board[6] == charToCheck && board[7] == charToCheck && board[8] == charToCheck) ||
           (board[0] == charToCheck && board[3] == charToCheck && board[6] == charToCheck) ||
           (board[2] == charToCheck && board[5] == charToCheck && board[8] == charToCheck);
}
