#include "masterserver.hpp"
#include <iostream>
#include <fstream>
#include "helpers.hpp"

MasterServer::MasterServer(): endServer(false) {}

void MasterServer::start() {
    std::ifstream serverListFile;
    serverListFile.open("serverlist");
    uint16_t webSocketPort;
    uint16_t serverUpdatePort;
    serverListFile >> TLSHelper::certificateChainFile;
    serverListFile >> TLSHelper::privateKeyFile;
    serverListFile >> TLSHelper::tmpDhFile;
    serverListFile >> webSocketPort;
    serverListFile >> serverUpdatePort;
    for(;;) {
        std::string serverName;
        std::string hostName;
        std::string hostPort;
        serverListFile >> serverName;
        if(serverListFile.eof()) {
            break;
        }
        serverListFile >> hostName;
        serverListFile >> hostPort;
        asio::ip::tcp::resolver resolver(ioContext);
        asio::ip::tcp::endpoint endPoint = *resolver.resolve(asio::ip::tcp::v4(), hostName, hostPort).begin();
        ServerData dataToAdd;
        dataToAdd.name = serverName;
        dataToAdd.host = hostName + ":" + hostPort;
        dataToAdd.lastUpdateTime = std::chrono::steady_clock::now();
        serverDataMap.emplace(endPoint.address(), dataToAdd);
    }
    serverListFile.close();
    update_data_buffers();

    webServer.set_access_channels(websocketpp::log::alevel::none);
    webServer.set_reuse_addr(true);
    webServer.init_asio();

    webServer.set_open_handler(websocketpp::lib::bind(&MasterServer::on_open,this,websocketpp::lib::placeholders::_1));
    webServer.set_close_handler(websocketpp::lib::bind(&MasterServer::on_close,this,websocketpp::lib::placeholders::_1));
    webServer.set_message_handler(websocketpp::lib::bind(&MasterServer::on_message,this,websocketpp::lib::placeholders::_1,websocketpp::lib::placeholders::_2));
    webServer.set_tls_init_handler(websocketpp::lib::bind(&TLSHelper::on_tls_init, MOZILLA_INTERMEDIATE, websocketpp::lib::placeholders::_1));

    std::thread commandThread(std::bind(&MasterServer::command_loop, this));
    std::thread mainLoopThread(std::bind(&MasterServer::main_loop, this, serverUpdatePort));
    run(webSocketPort);
    commandThread.join();
    mainLoopThread.join();
}

void MasterServer::on_open(connection_hdl hdl) {
    std::lock_guard<std::mutex> guard(connectionsLock);
    std::lock_guard<std::mutex> guard2(newConnectionBufferLock);
    try {
        webServer.send(hdl, newConnectionBuffer.data(), newConnectionBuffer.size(), websocketpp::frame::opcode::binary);
        connections.emplace(hdl, std::chrono::steady_clock::now());
    }
    catch(const websocketpp::exception& e) {
        std::cerr << "Error while sending opening message: " << e.what() << '\n';
    }
}

void MasterServer::on_close(connection_hdl hdl) {
    std::lock_guard<std::mutex> guard(connectionsLock);
    connections.erase(hdl);
}

void MasterServer::on_message(connection_hdl hdl, server::message_ptr /*msg*/) {
    std::lock_guard<std::mutex> guard(connectionsLock);
    auto it = connections.find(hdl);
    if(it != connections.end()) {
        it->second.lastRefresh = std::chrono::steady_clock::now();
    }
}

void MasterServer::command_loop() {
    for(;;) {
        std::string input;
        std::cin >> input;
        if(input == "stop") {
            std::cerr << "Stopping server...\n";
            break;
        }
    }
    endServer.store(true);
}

void MasterServer::main_loop(uint16_t serverUpdatePort) {
    lastRefreshTime = std::chrono::steady_clock::now();

    asio::ip::udp::socket serverUpdateSocket(ioContext, asio::ip::udp::endpoint(asio::ip::udp::v4(), serverUpdatePort));
    serverUpdateSocket.non_blocking(true);

    for(;;) {
        try {
            std::array<char, 4> dataReceived;
            asio::ip::udp::endpoint remoteEndpoint;
            serverUpdateSocket.receive_from(asio::buffer(dataReceived), remoteEndpoint);
            auto it = serverDataMap.find(remoteEndpoint.address());
            if(it != serverDataMap.end()) {
                std::string messageStr(dataReceived.data(), dataReceived.size());
                unsigned messageIndex = 0;
                it->second.numOfPlayersAtAddress = read_integer_from_string<int16_t>(messageStr, messageIndex);
                it->second.maxNumOfPlayersAtAddress = read_integer_from_string<int16_t>(messageStr, messageIndex);
                it->second.lastUpdateTime = std::chrono::steady_clock::now();
            }
            update_data_buffers();
        }
        catch(...) {}
        for(auto& mapPair : serverDataMap) {
            if(mapPair.second.numOfPlayersAtAddress != -1 && std::chrono::duration<double>(std::chrono::steady_clock::now() - mapPair.second.lastUpdateTime).count() >= SERVER_TIMEOUT_SECONDS) {
                mapPair.second.numOfPlayersAtAddress = -1;
                update_data_buffers();
            }
        }
        if(std::chrono::duration<double>(std::chrono::steady_clock::now() - lastRefreshTime).count() >= REFRESH_SECONDS) {
            update_connections();
            lastRefreshTime = std::chrono::steady_clock::now();
        }
        if(endServer.load()) {
            break;
        }
    }
    webServer.stop_listening();
    for(const auto& mapPair : connections) {
        try {
            webServer.close(mapPair.first, websocketpp::close::status::going_away, "Server closing");
        }
        catch(const websocketpp::exception& e) {
            std::cerr << "Error while closing: " << e.what() << '\n';
        }
        catch(...) {
        }
    }
}

void MasterServer::update_data_buffers() {
    std::lock_guard<std::mutex> guard(newConnectionBufferLock);
    updateBuffer.clear();
    newConnectionBuffer.clear();
    write_integer_to_buffer<uint16_t>(newConnectionBuffer, serverDataMap.size());
    for(auto mapPair : serverDataMap) {
        write_short_string_to_buffer(newConnectionBuffer, mapPair.second.name);
        write_short_string_to_buffer(newConnectionBuffer, mapPair.second.host);
        write_integer_to_buffer<int16_t>(newConnectionBuffer, mapPair.second.numOfPlayersAtAddress);
        write_integer_to_buffer<int16_t>(newConnectionBuffer, mapPair.second.maxNumOfPlayersAtAddress);
        write_integer_to_buffer<int16_t>(updateBuffer, mapPair.second.numOfPlayersAtAddress);
        write_integer_to_buffer<int16_t>(updateBuffer, mapPair.second.maxNumOfPlayersAtAddress);
    }
}

void MasterServer::update_connections() {
    std::lock_guard<std::mutex> guard(connectionsLock);
    for(auto& mapPair : connections) {
        if(!mapPair.second.timedOut) {
            if(std::chrono::duration<double>(std::chrono::steady_clock::now() - mapPair.second.lastRefresh).count() >= TIMEOUT_SECONDS) {
                try {
                    webServer.close(mapPair.first, websocketpp::close::status::normal, "Timeout");
                }
                catch(const websocketpp::exception& e) {
                    std::cerr << "Error closing connection: " << e.what() << '\n';
                }
                mapPair.second.timedOut = true;
            }
            else {
                try {
                    webServer.send(mapPair.first, updateBuffer.data(), updateBuffer.size(), websocketpp::frame::opcode::binary);
                }
                catch(const websocketpp::exception& e) {
                    std::cerr << "Error sending data to connection: " << e.what() << '\n';
                }
            }
        }
    }
}

void MasterServer::run(uint16_t port) {
    webServer.listen(port);
    webServer.start_accept();
    webServer.run();
}
