#include "gameplayer.hpp"
#include "helpers.hpp"
#include "utf8.h"

GamePlayer::GamePlayer(connection_hdl playerConHandler): firstMessage(true), conHandler(playerConHandler), input{false, false, false, false, false}, angle(0.0f), leftMsPressed(0.0f), rightMsPressed(0.0f), currentBonkCount(0), mostConsecutiveBonks(0), timeSinceLastMessage(std::chrono::steady_clock::now()) { }

GamePlayer::~GamePlayer() { }

void GamePlayer::add_received_message(const server::message_ptr& message) {
    messagesReceived.emplace(message);
}

void GamePlayer::add_message_to_send(const std::shared_ptr<std::vector<uint8_t>>& message) {
    if(!firstMessage && message && !message->empty()) {
        messagesToSend.emplace(message);
    }
}

void GamePlayer::send_queued_messages(server& webServer) {
    for(unsigned i = 0; i < messagesToSend.size(); i++) {
        const std::shared_ptr<std::vector<uint8_t>>& message = messagesToSend.front();
        try {
            webServer.send(conHandler, message->data(), message->size(), websocketpp::frame::opcode::binary);
        }
        catch(const websocketpp::exception& e) {
            std::cerr << "Error sending message to client: " << e.what() << '\n';
        }
        messagesToSend.pop();
    }
}

bool GamePlayer::is_pressed(GamePlayer::Button buttonToCheck) const {
    return input[static_cast<unsigned>(buttonToCheck)];
}

bool GamePlayer::get_is_shooting_and_clear() {
    bool isShooting = input[static_cast<unsigned>(GamePlayer::Button::SHOOT)];
    input[static_cast<unsigned>(GamePlayer::Button::SHOOT)] = false;
    return isShooting;
}

float GamePlayer::get_left_ms_pressed_and_clear() {
    float toRet = leftMsPressed;
    leftMsPressed = 0.0f;
    return toRet;
}

float GamePlayer::get_right_ms_pressed_and_clear() {
    float toRet = rightMsPressed;
    rightMsPressed = 0.0f;
    return toRet;
}

float GamePlayer::get_down_ms_pressed_and_clear() {
    float toRet = downMsPressed;
    downMsPressed = 0.0f;
    return toRet;
}

float GamePlayer::get_up_ms_pressed_and_clear() {
    float toRet = upMsPressed;
    upMsPressed = 0.0f;
    return toRet;
}

float GamePlayer::get_angle() const {
    return angle;
}

const std::string& GamePlayer::get_name() const {
    return name;
}

void GamePlayer::reset_current_bonk_count() {
    currentBonkCount = 0;
}

void GamePlayer::add_bonk() {
    currentBonkCount++;
    if(currentBonkCount > mostConsecutiveBonks) {
        mostConsecutiveBonks = currentBonkCount;
    }
}

unsigned GamePlayer::get_most_consecutive_bonks() const {
    return mostConsecutiveBonks;
}

bool GamePlayer::is_timed_out() const {
    return std::chrono::duration<float, std::milli>(std::chrono::steady_clock::now() - timeSinceLastMessage).count() > PLAYER_TIME_OUT_MS;
}

std::string GamePlayer::process_received_messages(GameServer& gameServer, World& world) {
    try {
        std::chrono::duration<float, std::milli> dur = std::chrono::steady_clock::now() - timeSinceLastMessage;
        float msSinceLastMessage = dur.count();
        if(msSinceLastMessage > PLAYER_TIME_OUT_MS) {
            return "Player timed out";
        }
        if(!messagesReceived.empty()) {
            timeSinceLastMessage = std::chrono::steady_clock::now();
            for(unsigned i = 0; i < messagesReceived.size(); i++) {
                server::message_ptr& message = messagesReceived.front();
                const std::string& messageStr = message->get_payload();
                unsigned int messageIndex = 0;
                if(firstMessage) {
                    firstMessage = false;

                    std::string tempName = read_short_string_from_string(messageStr, messageIndex);
                    unsigned strCodepointLength = utf8::distance(tempName.begin(), tempName.end());
                    if(strCodepointLength > NAME_CHAR_LIMIT) {
                        auto it = tempName.begin();
                        utf8::advance(it, NAME_CHAR_LIMIT, tempName.end());
                        tempName = std::string(tempName.begin(), it);
                    }

                    if(gameServer.player_name_taken(tempName)) {
                        return "Your name is taken";
                    }
                    name = tempName;

                    world.queueForTako.emplace_back(shared_from_this());
                    world.doQueueUpdate = true;
                    std::shared_ptr<std::vector<uint8_t>> dataBuffer(std::make_shared<std::vector<uint8_t>>());
                    world.write_new(*dataBuffer);
                    add_message_to_send(dataBuffer);
                    std::shared_ptr<std::vector<uint8_t>> tictactoeBuffer(std::make_shared<std::vector<uint8_t>>());
                    world.tictactoe.write_new(*tictactoeBuffer);
                    add_message_to_send(tictactoeBuffer);
                    add_message_to_send(world.scoreboardMessageBuffer);
                    add_message_to_send(world.queueMessageBuffer);
                }
                else {
                    char typeOfMessage = messageStr.at(messageIndex);
                    messageIndex += 1;
                    if(typeOfMessage == 'i') {
                        uint8_t buttonInputs = messageStr.at(messageIndex);
                        for(unsigned int i = 0; i < 3; i++) {
                            input[i] = (buttonInputs >> i) & 1;
                        }
                        messageIndex += 1;
                        angle = read_integer_from_string<int16_t>(messageStr, messageIndex);
                        if(angle < 0.0f) {
                            angle = clamp_value(angle, -SHOOT_ANGLE_MAXIMUM, -SHOOT_ANGLE_MINIMUM);
                        }
                        else {
                            angle = clamp_value(angle, SHOOT_ANGLE_MINIMUM, SHOOT_ANGLE_MAXIMUM);
                        }
                        leftMsPressed = clamp_value(static_cast<float>(read_integer_from_string<uint16_t>(messageStr, messageIndex)), 0.0f, msSinceLastMessage);
                        rightMsPressed = clamp_value(static_cast<float>(read_integer_from_string<uint16_t>(messageStr, messageIndex)), 0.0f, msSinceLastMessage);
                        upMsPressed = clamp_value(static_cast<float>(read_integer_from_string<uint16_t>(messageStr, messageIndex)), 0.0f, msSinceLastMessage);
                        downMsPressed = clamp_value(static_cast<float>(read_integer_from_string<uint16_t>(messageStr, messageIndex)), 0.0f, msSinceLastMessage);
                    }
                    else if(typeOfMessage == 't') {
                        uint8_t slotSelectedOnBoard = messageStr.at(messageIndex);
                        messageIndex += 1;
                        world.tictactoe.set_vote(shared_from_this(), slotSelectedOnBoard);
                    }
                    else if(typeOfMessage != 'r') {
                        return "Invalid message type";
                    }
                }
                messagesReceived.pop();
            }
        }
    }
    catch(...) {
        return "Invalid payload";
    }
    return "";
}

const connection_hdl& GamePlayer::get_connection() const {
    return conHandler;
}
