#pragma once
#include <cstdint>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <chrono>

class WorldObject {
public:
    WorldObject();
    virtual ~WorldObject();
    virtual void write_new(std::vector<uint8_t>& dataBuffer);
    virtual void write_update(std::vector<uint8_t>& dataBuffer);
    uint8_t worldObjectID;
    bool newObject;
    static bool is_temporary_reserved(uint8_t idToCheck);
    static std::unordered_set<uint8_t> reservedWorldIDs;
    static std::unordered_map<uint8_t, std::chrono::steady_clock::time_point> timeSinceDestruction;
    static uint8_t nextIDToAssign;
    static const double secFromDestructionToFreeID;
};
