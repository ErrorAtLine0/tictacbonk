#include "helpers.hpp"

std::random_device RNG::rd;
std::mt19937 RNG::mt(RNG::rd());

// From: https://stackoverflow.com/a/1001373
bool is_big_endian()
{
    union {
        uint32_t i;
        char c[4];
    } bint = {0x01020304};

    return bint.c[0] == 1;
}

std::string read_short_string_from_string(const std::string& dataBuffer, unsigned& indexToReadAt) {
    uint8_t strLength = dataBuffer.at(indexToReadAt);
    indexToReadAt += 1;
    std::string shortStr = dataBuffer.substr(indexToReadAt, strLength);
    indexToReadAt += strLength;
    return shortStr;
}

void write_short_string_to_buffer(std::vector<uint8_t>& dataBuffer, const std::string& strToWrite) {
    dataBuffer.emplace_back(strToWrite.length());
    const uint8_t* bytePtr = reinterpret_cast<const uint8_t*>(strToWrite.data());
    for(unsigned i = 0; i < strToWrite.length(); i++) {
        dataBuffer.emplace_back(bytePtr[i]);
    }
}
