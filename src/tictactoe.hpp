#pragma once
#include <cstdint>
#include <vector>
#include <array>
#include <unordered_map>
#include <memory>

class GamePlayer;

class TicTacToe {
public:
    TicTacToe();
    void update(double deltaTime);
    void write_new(std::vector<uint8_t>& dataBuffer);
    void write_update(std::vector<uint8_t>& dataBuffer);
    void set_vote(const std::shared_ptr<GamePlayer>& player, unsigned voteIndex);
    void remove_vote(const std::shared_ptr<GamePlayer>& player);
    bool get_has_update_and_clear();
    char get_winning_char_and_clear();
private:
    void write_common(std::vector<uint8_t>& dataBuffer);
    bool check_if_winner(char charToCheck);

    std::array<unsigned, 9> voteCount;
    unsigned winningSlot;
    char winningChar;
    bool winnerChosen;
    bool updateReadyToSend;

    std::unordered_map<std::shared_ptr<GamePlayer>, unsigned> votes;
    std::array<char, 9> board;
    enum class TicTacToeState : unsigned {
        VOTE = 0,
        RESULT = 1,
        REST = 2
    } state;
    double timeInState;
};
