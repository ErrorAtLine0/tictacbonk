#include "gameserver.hpp"
#include "gameplayer.hpp"
#include <iostream>
#include <functional>
#include <fstream>
#include "helpers.hpp"

GameServer::GameServer(): endServer(false), playerAtomicSize(0), timeSinceLastPacket(0.0), timeSinceLastScoreUpdate(0.0) {}

bool GameServer::player_name_taken(const std::string& playerName) {
    for(const std::shared_ptr<GamePlayer>& player : players) {
        if(player->get_name() == playerName) {
            return true;
        }
    }
    return false;
}

void GameServer::start() {
    std::ifstream serverPropertiesFile;
    serverPropertiesFile.open("serverproperties");
    uint16_t port;
    std::string serverIP;
    std::string serverPort;
    serverPropertiesFile >> TLSHelper::certificateChainFile;
    serverPropertiesFile >> TLSHelper::privateKeyFile;
    serverPropertiesFile >> TLSHelper::tmpDhFile;
    serverPropertiesFile >> port;
    serverPropertiesFile >> serverIP;
    serverPropertiesFile >> serverPort;
    serverPropertiesFile >> serverPlayerLimit;
    serverPropertiesFile.close();

    webServer.set_access_channels(websocketpp::log::alevel::none);
    webServer.set_reuse_addr(true);
    webServer.init_asio();

    webServer.set_open_handler(websocketpp::lib::bind(&GameServer::on_open,this,websocketpp::lib::placeholders::_1));
    webServer.set_close_handler(websocketpp::lib::bind(&GameServer::on_close,this,websocketpp::lib::placeholders::_1));
    webServer.set_message_handler(websocketpp::lib::bind(&GameServer::on_message,this,websocketpp::lib::placeholders::_1,websocketpp::lib::placeholders::_2));
    webServer.set_tls_init_handler(websocketpp::lib::bind(&TLSHelper::on_tls_init, MOZILLA_INTERMEDIATE, websocketpp::lib::placeholders::_1));

    std::thread serverListThread(std::bind(&GameServer::server_list_socket_loop, this, serverIP, serverPort));
    std::thread commandThread(std::bind(&GameServer::command_loop, this));
    std::thread mainLoopThread(std::bind(&GameServer::main_loop, this));
    run(port);
    serverListThread.join();
    commandThread.join();
    mainLoopThread.join();
}

void GameServer::on_open(connection_hdl hdl) {
    std::lock_guard<std::mutex> guard(actionsLock);
    actions.emplace(ActionType::OPEN, hdl);
}

void GameServer::on_close(connection_hdl hdl) {
    std::lock_guard<std::mutex> guard(actionsLock);
    actions.emplace(ActionType::CLOSE, hdl);
}

void GameServer::on_message(connection_hdl hdl, server::message_ptr msg) {
    std::lock_guard<std::mutex> guard(actionsLock);
    actions.emplace(ActionType::MESSAGE, hdl, msg);
}

void GameServer::command_loop() {
    for(;;) {
        std::string input;
        std::cin >> input;
        if(input == "stop") {
            std::cerr << "Stopping server...\n";
            break;
        }
    }
    endServer.store(true);
}

void GameServer::server_list_socket_loop(const std::string& serverListIP, const std::string& serverListPort) {
    asio::io_context ioContext;
    asio::ip::udp::resolver resolver(ioContext);
    asio::ip::udp::endpoint receiverEndpoint = *resolver.resolve(asio::ip::udp::v4(), serverListIP, serverListPort).begin();
    asio::ip::udp::socket serverListSocket(ioContext);
    serverListSocket.open(asio::ip::udp::v4());
    std::chrono::time_point<std::chrono::steady_clock> lastUpdateTime = std::chrono::steady_clock::now();
    while(!endServer.load()) {
        if(std::chrono::duration<double>(std::chrono::steady_clock::now() - lastUpdateTime).count() >= SECONDS_BETWEEN_LIST_UPDATES) {
            lastUpdateTime = std::chrono::steady_clock::now();
            std::vector<uint8_t> dataBuffer;
            write_integer_to_buffer<int16_t>(dataBuffer, playerAtomicSize.load());
            write_integer_to_buffer<int16_t>(dataBuffer, serverPlayerLimit);
            serverListSocket.send_to(asio::buffer(dataBuffer), receiverEndpoint);
        }
    }
    std::vector<uint8_t> dataBuffer;
    write_integer_to_buffer<int16_t>(dataBuffer, -1);
    write_integer_to_buffer<int16_t>(dataBuffer, serverPlayerLimit);
    serverListSocket.send_to(asio::buffer(dataBuffer), receiverEndpoint);
}

void GameServer::main_loop() {
    previousTime = std::chrono::steady_clock::now();

    while(!endServer.load()) {
        auto currentTime = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed = currentTime - previousTime;
        double deltaTime = elapsed.count();
        playerAtomicSize.store(players.size());
        world.update(deltaTime);
        process_actions();
        std::deque<std::shared_ptr<GamePlayer>>::iterator playerIt = players.begin();
        while(playerIt != players.end()) {
            std::shared_ptr<GamePlayer>& player = (*playerIt);
            std::string str = player->process_received_messages(*this, world);
            if(!str.empty()) {
                remove_player_with_pointer(player, str);
                playerIt = players.begin();
            }
            else {
                ++playerIt;
            }
        }
        if(world.tictactoe.get_has_update_and_clear()) {
            std::shared_ptr<std::vector<uint8_t>> dataBuffer(std::make_shared<std::vector<uint8_t>>());
            world.tictactoe.write_update(*dataBuffer);
            for(std::shared_ptr<GamePlayer>& player : players) {
                player->add_message_to_send(dataBuffer);
            }
        }
        timeSinceLastPacket += deltaTime;
        if(timeSinceLastPacket >= SECONDS_BETWEEN_UPDATES) {
            timeSinceLastPacket -= SECONDS_BETWEEN_UPDATES;
            std::shared_ptr<std::vector<uint8_t>> dataBuffer(std::make_shared<std::vector<uint8_t>>());
            world.write_update(*dataBuffer);
            for(std::shared_ptr<GamePlayer>& player : players) {
                player->add_message_to_send(dataBuffer);
            }
        }
        timeSinceLastScoreUpdate += deltaTime;
        if(timeSinceLastScoreUpdate >= SECONDS_BETWEEN_SCORE_UPDATES) {
            timeSinceLastScoreUpdate -= SECONDS_BETWEEN_SCORE_UPDATES;
            if(world.update_scoreboard(players)) {
                for(std::shared_ptr<GamePlayer>& player : players) {
                    player->add_message_to_send(world.scoreboardMessageBuffer);
                }
            }
        }
        if(world.doQueueUpdate && world.update_queue_message()) {
            for(std::shared_ptr<GamePlayer>& player : players) {
                player->add_message_to_send(world.queueMessageBuffer);
            }
        }
        for(std::shared_ptr<GamePlayer>& player : players) {
            player->send_queued_messages(webServer);
        }
        previousTime = currentTime;
    }
    webServer.stop_listening();
    for(const auto& mapPair : connections) {
        try {
            webServer.close(mapPair.first, websocketpp::close::status::going_away, "Server closing");
        }
        catch(const websocketpp::exception& e) {
            std::cerr << "Error while closing connection while closing server: " << e.what() << '\n';
        }
        catch(...) {}
    }
}

void GameServer::process_actions() {
    std::lock_guard<std::mutex> guard(actionsLock);
    unsigned numOfPacketsToProcess = (actions.size() < MAX_PACKETS_TO_PROCESS) ? actions.size() : MAX_PACKETS_TO_PROCESS;
    for(unsigned i = 0; i < numOfPacketsToProcess; i++) {
        const Action& a = actions.front();
        switch(a.type) {
            case ActionType::OPEN: {
                if(players.size() < static_cast<unsigned>(serverPlayerLimit)) {
                    std::shared_ptr<GamePlayer> gamePlayer = std::make_shared<GamePlayer>(a.hdl);
                    if(connections.emplace(a.hdl, gamePlayer).second) {
                        players.emplace_back(gamePlayer);
                    }
                    else {
                        try {
                            webServer.close(a.hdl, websocketpp::close::status::normal, "Failed to add player");
                        }
                        catch(const websocketpp::exception& e) {
                            std::cerr << "Error while closing connection: " << e.what() << '\n';
                        }
                        catch(...) {}
                    }
                }
                else {
                    try {
                        webServer.close(a.hdl, websocketpp::close::status::normal, "Server full");
                    }
                    catch(const websocketpp::exception& e) {
                        std::cerr << "Error while closing connection: " << e.what() << '\n';
                    }
                    catch(...) {}
                }
                break;
            }
            case ActionType::CLOSE: {
                remove_player_with_con_hdl(a.hdl, "");
                break;
            }
            case ActionType::MESSAGE: {
                auto it = connections.find(a.hdl);
                if(it != connections.end()) {
                    it->second->add_received_message(a.msg);
                }
                break;
            }
        }
        actions.pop();
    }
}

void GameServer::remove_player_common(const std::shared_ptr<GamePlayer>& playerToRemove, const std::string& reasonToRemove) {
    if(!reasonToRemove.empty()) {
        try {
            webServer.close(playerToRemove->get_connection(), websocketpp::close::status::normal, reasonToRemove);
        }
        catch(const websocketpp::exception& e) {
            std::cerr << "Error while closing connection: " << e.what() << '\n';
        }
        catch(...) {}
    }
    players.erase(std::remove(players.begin(), players.end(), playerToRemove), players.end());
    world.queueForTako.erase(std::remove(world.queueForTako.begin(), world.queueForTako.end(), playerToRemove), world.queueForTako.end());
    world.tictactoe.remove_vote(playerToRemove);
    world.takos.erase(std::remove_if(world.takos.begin(), world.takos.end(), [&playerToRemove](Tako& tako) { return tako.get_controller() == playerToRemove; }), world.takos.end());
    world.doQueueUpdate = true;
}

void GameServer::remove_player_with_pointer(const std::shared_ptr<GamePlayer>& playerToRemove, const std::string& reasonToRemove) {
    auto it = std::find_if(connections.begin(), connections.end(), [&playerToRemove](const std::pair<connection_hdl, std::shared_ptr<GamePlayer>>& p) { return playerToRemove == p.second; });
    if(it != connections.end()) {
        connections.erase(it);
    }
    remove_player_common(playerToRemove, reasonToRemove);
}

void GameServer::remove_player_with_con_hdl(const connection_hdl& hdl, const std::string& reasonToRemove) {
    auto it = connections.find(hdl);
    if(it != connections.end()) {
        connections.erase(it);
        remove_player_common(it->second, reasonToRemove);
    }
}

void GameServer::run(uint16_t port) {
    webServer.listen(port);
    webServer.start_accept();
    webServer.run();
}
