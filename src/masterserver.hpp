#pragma once
#include <map>
#include <chrono>
#include <deque>
#include <queue>
#include <atomic>
#include <thread>
#include <unordered_map>
#include "tlssetup.hpp"

using websocketpp::connection_hdl;

class MasterServer {
public:
    MasterServer();
    void start();
private:
    struct ConnectionData {
        ConnectionData(const std::chrono::time_point<std::chrono::steady_clock>& initTime): lastRefresh(initTime) {}
        std::chrono::time_point<std::chrono::steady_clock> lastRefresh;
        bool timedOut = false;
    };
    typedef std::map<connection_hdl, ConnectionData, std::owner_less<connection_hdl>> connectionList;
    void on_open(connection_hdl hdl);
    void on_close(connection_hdl hdl);
    void on_message(connection_hdl hdl, server::message_ptr /*msg*/);
    void update_connections();
    void main_loop(uint16_t serverUpdatePort);
    void run(uint16_t port);
    void command_loop();
    void update_data_buffers();

    const double SERVER_TIMEOUT_SECONDS = 3.0;
    const double TIMEOUT_SECONDS = 7.0;
    const double REFRESH_SECONDS = 0.5;

    struct ServerData {
        std::string name;
        std::string host;
        int16_t numOfPlayersAtAddress = 0;
        int16_t maxNumOfPlayersAtAddress = 0;
        std::chrono::time_point<std::chrono::steady_clock> lastUpdateTime;
    };

    asio::io_context ioContext;
    std::vector<uint8_t> newConnectionBuffer;
    std::vector<uint8_t> updateBuffer;
    std::unordered_map<asio::ip::address, ServerData> serverDataMap;
    server webServer;
    std::mutex newConnectionBufferLock;
    std::mutex connectionsLock;
    connectionList connections;
    std::atomic<bool> endServer;
    std::chrono::time_point<std::chrono::steady_clock> lastRefreshTime;
};
