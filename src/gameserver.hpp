#pragma once
#include "world.hpp"
#include "tlssetup.hpp"
#include <map>
#include <chrono>
#include <deque>
#include <queue>
#include <atomic>
#include <thread>

using websocketpp::connection_hdl;

class GamePlayer;

class GameServer {
public:
    GameServer();
    void start();
    bool player_name_taken(const std::string& playerName);
private:
    typedef std::map<connection_hdl, std::shared_ptr<GamePlayer>, std::owner_less<connection_hdl>> connectionList;
    void on_open(connection_hdl hdl);
    void on_close(connection_hdl hdl);
    void on_message(connection_hdl hdl, server::message_ptr msg);
    void server_list_socket_loop(const std::string& serverListIP, const std::string& serverListPort);
    void main_loop();
    void process_actions();
    void remove_player_common(const std::shared_ptr<GamePlayer>& playerToRemove, const std::string& reasonToRemove);
    void remove_player_with_pointer(const std::shared_ptr<GamePlayer>& playerToRemove, const std::string& reasonToRemove);
    void remove_player_with_con_hdl(const connection_hdl& hdl, const std::string& reasonToRemove);
    void run(uint16_t port);
    void command_loop();

    const unsigned MAX_PACKETS_TO_PROCESS = 10;
    const double SECONDS_BETWEEN_UPDATES = 0.1;
    const double SECONDS_BETWEEN_LIST_UPDATES = 0.1;
    const double SECONDS_BETWEEN_SCORE_UPDATES = 2.0;
    enum class ActionType {
        OPEN,
        CLOSE,
        MESSAGE
    };
    struct Action {
        Action(ActionType t, connection_hdl h): type(t), hdl(h) { }
        Action(ActionType t, connection_hdl h, server::message_ptr m): type(t), hdl(h), msg(m) { }
        ActionType type;
        connection_hdl hdl;
        server::message_ptr msg;
    };

    server webServer;
    connectionList connections;
    std::atomic<bool> endServer;
    std::atomic<int16_t> playerAtomicSize;
    int16_t serverPlayerLimit;
    std::mutex actionsLock;
    std::queue<Action> actions;

    World world;
    std::deque<std::shared_ptr<GamePlayer>> players;

    std::chrono::time_point<std::chrono::steady_clock> previousTime;
    double timeSinceLastPacket;

    double timeSinceLastScoreUpdate;
};
