#include "crowbar.hpp"
#include "helpers.hpp"
#include "gameplayer.hpp"

Crowbar::Crowbar(Vector2 initPos, Vector2 initVelocity, const std::shared_ptr<GamePlayer>& initThrower): pos(initPos), velocity(initVelocity), lifetime(0.0), drawCircleOnSummon(false), thrower(initThrower) {}

Crowbar::Crowbar(Vector2 initPos, Vector2 initVelocity): pos(initPos), velocity(initVelocity), lifetime(0.0), drawCircleOnSummon(true) {}

Crowbar::~Crowbar() {}

bool Crowbar::update(double deltaTime) {
    if(pos[1] == 0.0f) {
        velocity[0] = 0.0f;
        velocity[1] = 0.0f;
        lifetime += deltaTime;
        if(lifetime >= CROWBAR_LIFETIME) {
            return false;
        }
    }
    else {
        pos[0] += velocity[0] * deltaTime;
        pos[1] += velocity[1] * deltaTime;
        velocity[1] -= GRAVITY * deltaTime;
        if(pos[0] <= 0.0f) {
            pos[0] = 0.0f;
            velocity[0] = -velocity[0];
        }
        else if(pos[0] >= ARENA_SIZE_X - CROWBAR_SIZE) {
            pos[0] = ARENA_SIZE_X - CROWBAR_SIZE;
            velocity[0] = -velocity[0];
        }
        if(pos[1] <= 0.0f) {
            pos[1] = 0.0f;
        }
    }
    return true;
}

void Crowbar::write_new(std::vector<uint8_t>& dataBuffer) {
    WorldObject::write_new(dataBuffer);
    dataBuffer.emplace_back(drawCircleOnSummon ? 'd' : 'c');
    write_common(dataBuffer);
}

void Crowbar::write_update(std::vector<uint8_t>& dataBuffer) {
    WorldObject::write_update(dataBuffer);
    write_common(dataBuffer);
}

void Crowbar::write_common(std::vector<uint8_t>& dataBuffer) {
    write_integer_to_buffer<uint16_t>(dataBuffer, static_cast<uint16_t>(pos[0]));
    write_integer_to_buffer<uint16_t>(dataBuffer, static_cast<uint16_t>(pos[1]));
    write_integer_to_buffer<uint16_t>(dataBuffer, static_cast<uint16_t>(velocity[0]));
    write_integer_to_buffer<uint16_t>(dataBuffer, static_cast<uint16_t>(velocity[1]));
}

const std::weak_ptr<GamePlayer>& Crowbar::get_thrower() const {
    return thrower;
}

const Vector2& Crowbar::get_pos() const {
    return pos;
}
