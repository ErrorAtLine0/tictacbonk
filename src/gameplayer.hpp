#pragma once
#include "gameserver.hpp"
#include "world.hpp"
#include <queue>
#include <string>
#include <memory>

class GamePlayer : public std::enable_shared_from_this<GamePlayer> {
public:
    GamePlayer(connection_hdl playerConHandler);
    GamePlayer(const GamePlayer&) = delete;
    ~GamePlayer();
    std::string process_received_messages(GameServer& gameServer, World& world);
    void add_received_message(const server::message_ptr& message);
    void add_message_to_send(const std::shared_ptr<std::vector<uint8_t>>& message);
    void send_queued_messages(server& webServer);
    const connection_hdl& get_connection() const;
    enum class Button : unsigned {
        LEFT = 0,
        RIGHT = 1,
        SHOOT = 2
    };
    bool is_pressed(Button buttonToCheck) const;
    const std::string& get_name() const;
    float get_angle() const;
    bool get_is_shooting_and_clear();
    float get_left_ms_pressed_and_clear();
    float get_right_ms_pressed_and_clear();
    float get_down_ms_pressed_and_clear();
    float get_up_ms_pressed_and_clear();
    float get_ms_between_messages() const;
    void reset_current_bonk_count();
    void add_bonk();
    unsigned get_most_consecutive_bonks() const;
    bool is_timed_out() const;
private:
    bool firstMessage;
    std::string name;
    connection_hdl conHandler;
    std::queue<server::message_ptr> messagesReceived;
    std::queue<std::shared_ptr<std::vector<uint8_t>>> messagesToSend;
    std::array<bool, 5> input;
    float angle;
    float leftMsPressed;
    float rightMsPressed;
    float downMsPressed;
    float upMsPressed;
    unsigned currentBonkCount;
    unsigned mostConsecutiveBonks;
    std::chrono::time_point<std::chrono::steady_clock> timeSinceLastMessage;
};
