#pragma once
#include "constants.hpp"
#include "worldobject.hpp"
#include <list>
#include <array>
#include <memory>

class GamePlayer;
class World;

class Tako : public WorldObject {
public:
    Tako(float spawnXPos, const std::shared_ptr<GamePlayer>& initController);
    Tako(const Tako&) = delete;
    bool update(double deltaTime, World& world);
    void mark_to_melt();
    const std::shared_ptr<GamePlayer>& get_controller() const;
    virtual void write_new(std::vector<uint8_t>& dataBuffer) override;
    virtual void write_update(std::vector<uint8_t>& dataBuffer) override;

    ~Tako();
private:
    void down_pressed(float durationSincePress);
    void up_pressed(float durationSincePress, float typicalDelta);
    void write_common(std::vector<uint8_t>& dataBuffer);
    Vector2 pos;
    Vector2 velocity;
    double timeWithoutCrowbar;
    double timeAfterBonk;
    double timeSinceMarked;
    bool melting;
    std::shared_ptr<GamePlayer> controller;
};
