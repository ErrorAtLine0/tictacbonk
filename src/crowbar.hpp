#pragma once
#include "constants.hpp"
#include "worldobject.hpp"
#include <memory>

class GamePlayer;

class Crowbar : public WorldObject {
public:
    Crowbar(Vector2 initPos, Vector2 initVelocity, const std::shared_ptr<GamePlayer>& initThrower);
    Crowbar(Vector2 initPos, Vector2 initVelocity);
    Crowbar(const Crowbar&) = delete;
    ~Crowbar();
    bool update(double deltaTime);
    virtual void write_new(std::vector<uint8_t>& dataBuffer) override;
    virtual void write_update(std::vector<uint8_t>& dataBuffer) override;
    const std::weak_ptr<GamePlayer>& get_thrower() const;
    const Vector2& get_pos() const;
private:
    void write_common(std::vector<uint8_t>& dataBuffer);
    Vector2 pos;
    Vector2 velocity;
    double lifetime;
    bool drawCircleOnSummon;
    std::weak_ptr<GamePlayer> thrower;
};
