#include "world.hpp"
#include "helpers.hpp"
#include "gameplayer.hpp"
#include <queue>

void World::update(double deltaTime) {
    tictactoe.update(deltaTime);
    char winningTicTacToeChar = tictactoe.get_winning_char_and_clear();
    if(winningTicTacToeChar == 'X') {
        for(Tako& tako : takos) {
            tako.mark_to_melt();
        }
    }
    else if(winningTicTacToeChar == 'O') {
        Vector2 posToPass;
        Vector2 velToPass;
        posToPass[0] = 0.0f;
        posToPass[1] = 500.0f;
        velToPass[0] = 2500.0f;
        velToPass[1] = 2500.0f;
        crowbars.emplace_back(posToPass, velToPass);
        posToPass[1] = 1500.0f;
        crowbars.emplace_back(posToPass, velToPass);
        posToPass[1] = 2500.0f;
        crowbars.emplace_back(posToPass, velToPass);
        posToPass[0] = ARENA_SIZE_X;
        velToPass[0] = -velToPass[0];
        crowbars.emplace_back(posToPass, velToPass);
        posToPass[1] = 1500.0f;
        crowbars.emplace_back(posToPass, velToPass);
        posToPass[1] = 500.0f;
        crowbars.emplace_back(posToPass, velToPass);
    }
    while(takos.size() < MAX_TAKOS_IN_ARENA && !queueForTako.empty()) {
        takos.emplace_back(RNG::get_random_real_number(0.0f, static_cast<float>(ARENA_SIZE_X - TAKO_SIZE)), queueForTako.front());
        queueForTako.pop_front();
        doQueueUpdate = true;
    }
    std::list<Crowbar>::iterator crowbarIt = crowbars.begin();
    while(crowbarIt != crowbars.end()) {
        Crowbar& crowbar = (*crowbarIt);
        if(!crowbar.update(deltaTime)) {
            crowbarIt = crowbars.erase(crowbarIt);
        }
        else {
            ++crowbarIt;
        }
    }
    std::list<Tako>::iterator takoIt = takos.begin();
    while(takoIt != takos.end()) {
        Tako& tako = (*takoIt);
        if(!tako.update(deltaTime, *this)) {
            takoIt = takos.erase(takoIt);
        }
        else {
            ++takoIt;
        }
    }
    while(crowbars.size() > CROWBAR_LIMIT) {
        crowbars.pop_back();
    }
}

bool World::update_queue_message() {
    std::vector<uint8_t> oldMessageBuffer = *queueMessageBuffer;
    queueMessageBuffer->clear();
    queueMessageBuffer->emplace_back('q');
    unsigned queueLength = MAX_PLAYERS_ON_QUEUE_DISPLAY < queueForTako.size() ? MAX_PLAYERS_ON_QUEUE_DISPLAY : queueForTako.size();
    write_integer_to_buffer<uint8_t>(*queueMessageBuffer, queueLength);
    auto it = queueForTako.cbegin();
    for(unsigned i = 0; i < queueLength; i++) {
        const std::shared_ptr<GamePlayer>& p = *it;
        write_short_string_to_buffer(*queueMessageBuffer, p->get_name());
        it++;
    }
    doQueueUpdate = false;
    return oldMessageBuffer != *queueMessageBuffer;
}

bool World::update_scoreboard(const std::deque<std::shared_ptr<GamePlayer>>& players) {
    std::vector<uint8_t> oldMessageBuffer = *scoreboardMessageBuffer;
    scoreboardMessageBuffer->clear();
    scoreboardMessageBuffer->emplace_back('s');
    auto cmp = [](const std::shared_ptr<GamePlayer>& p1, const std::shared_ptr<GamePlayer>& p2) {
        return p1->get_most_consecutive_bonks() < p2->get_most_consecutive_bonks();
    };
    std::priority_queue<std::shared_ptr<GamePlayer>, std::deque<std::shared_ptr<GamePlayer>>, decltype(cmp)> orderedQueue(cmp, players);
    unsigned scoreboardLength = MAX_PLAYERS_ON_SCOREBOARD < players.size() ? MAX_PLAYERS_ON_SCOREBOARD : players.size();
    write_integer_to_buffer<uint16_t>(*scoreboardMessageBuffer, scoreboardLength);
    for(unsigned i = 0; i < scoreboardLength; i++) {
        const std::shared_ptr<GamePlayer>& p = orderedQueue.top();
        write_short_string_to_buffer(*scoreboardMessageBuffer, p->get_name());
        write_integer_to_buffer<uint16_t>(*scoreboardMessageBuffer, p->get_most_consecutive_bonks());
        orderedQueue.pop();
    }
    return oldMessageBuffer != *scoreboardMessageBuffer;
}

void World::write_new(std::vector<uint8_t>& dataBuffer) {
    dataBuffer.emplace_back('c');
    // Dont write new objects, as player will receive them in the future
    for(Tako& tako : takos) {
        if(!tako.newObject) {
            tako.write_new(dataBuffer);
        }
    }
    for(Crowbar& crowbar : crowbars) {
        if(!crowbar.newObject) {
            crowbar.write_new(dataBuffer);
        }
    }
}

void World::write_update(std::vector<uint8_t>& dataBuffer) {
    dataBuffer.emplace_back('u');
    for(Tako& tako : takos) {
        if(tako.newObject) {
            tako.write_new(dataBuffer);
            tako.newObject = false;
        }
        else {
            tako.write_update(dataBuffer);
        }
    }
    for(Crowbar& crowbar : crowbars) {
        if(crowbar.newObject) {
            crowbar.write_new(dataBuffer);
            crowbar.newObject = false;
        }
        else {
            crowbar.write_update(dataBuffer);
        }
    }
}
