#include "tako.hpp"
#include "gameplayer.hpp"
#include "helpers.hpp"
#include <cmath>

Tako::Tako(float spawnXPos, const std::shared_ptr<GamePlayer>& initController): pos{spawnXPos, ARENA_SIZE_Y}, velocity{0.0f, 0.0f}, timeWithoutCrowbar(0.0), timeAfterBonk(0.0), timeSinceMarked(0.0), melting(false), controller(initController) {
    std::shared_ptr<std::vector<uint8_t>> dataBuffer(std::make_shared<std::vector<uint8_t>>());
    dataBuffer->emplace_back('i');
    dataBuffer->emplace_back(worldObjectID);
    initController->add_message_to_send(dataBuffer);
    controller->reset_current_bonk_count();
}

bool Tako::update(double deltaTime, World& world) {
    if(!controller || controller->is_timed_out()) {
        return false;
    }
    if(timeAfterBonk != 0.0) {
        timeAfterBonk += deltaTime;
        if(timeAfterBonk >= TAKO_TIME_AFTER_DEATH) {
            world.queueForTako.emplace_back(controller);
            return false;
        }
    }
    else {
        if(timeWithoutCrowbar != 0.0) {
            timeWithoutCrowbar += deltaTime;
            if(timeWithoutCrowbar >= CROWBAR_RECOVERY_TIME) {
                timeWithoutCrowbar = 0.0;
            }
        }
        if(timeSinceMarked != 0.0) {
            timeSinceMarked += deltaTime;
            if(timeSinceMarked >= TAKO_MARK_TIME) {
                timeAfterBonk += deltaTime;
                melting = true;
            }
        }
        float previousVelocityX = velocity[0];
        if(controller->is_pressed(GamePlayer::Button::LEFT) && !controller->is_pressed(GamePlayer::Button::RIGHT)) { // Move Left
            velocity[0] = -TAKO_SPEED;
        }
        else if(!controller->is_pressed(GamePlayer::Button::LEFT) && controller->is_pressed(GamePlayer::Button::RIGHT)) { // Move Right
            velocity[0] = TAKO_SPEED;
        }
        else {
            velocity[0] = 0.0f;
        }
        float upMsPressed = controller->get_up_ms_pressed_and_clear() / 1000.0f;
        float downMsPressed = controller->get_down_ms_pressed_and_clear() / 1000.0f;
        if(upMsPressed >= downMsPressed) {
            up_pressed(upMsPressed, deltaTime);
            down_pressed(downMsPressed);
        }
        else {
            down_pressed(downMsPressed);
            up_pressed(upMsPressed, deltaTime);
        }
        if(controller->get_is_shooting_and_clear() && timeWithoutCrowbar == 0.0) {
            Vector2 posToPass;
            posToPass[0] = pos[0] + TAKO_SIZE / 2.0f - CROWBAR_SIZE / 2.0f;
            posToPass[1] = pos[1] + TAKO_SIZE / 2.0f - CROWBAR_SIZE / 2.0f;
            Vector2 velToPass;
            const float shootAngle = controller->get_angle();
            velToPass[0] = (shootAngle < 0.0f ? -1.0f : 1.0f) * CROWBAR_SPEED * std::cos(std::fabs(shootAngle) * PI_CONSTANT/180.0);
            velToPass[1] = CROWBAR_SPEED * std::sin(std::fabs(shootAngle) * PI_CONSTANT/180.0);
            world.crowbars.emplace_back(posToPass, velToPass, controller);
            timeWithoutCrowbar += deltaTime;
        }
        std::list<Crowbar>::iterator it = world.crowbars.begin();
        while(it != world.crowbars.end()) {
            Crowbar& crowbar = (*it);
            bool removeCrowbar = false;
            // AABB Collision check between crowbar and tako
            if(crowbar.get_pos()[0] <= pos[0] + TAKO_SIZE && crowbar.get_pos()[0] + CROWBAR_SIZE >= pos[0] &&
                crowbar.get_pos()[1] <= pos[1] + TAKO_SIZE && crowbar.get_pos()[1] + CROWBAR_SIZE >= pos[1]) {
                if(crowbar.get_pos()[1] == 0.0f) {
                    if(timeWithoutCrowbar != 0.0) {
                        timeWithoutCrowbar = 0.0;
                        removeCrowbar = true;
                    }
                }
                else if(crowbar.get_thrower().expired() || crowbar.get_thrower().lock() != controller) {
                    timeAfterBonk += deltaTime;
                    if(!crowbar.get_thrower().expired()) {
                        crowbar.get_thrower().lock()->add_bonk();
                    }
                    removeCrowbar = true;
                }
            }
            if(removeCrowbar) {
                it = world.crowbars.erase(it);
            }
            else {
                ++it;
            }
        }
        velocity[1] -= GRAVITY * deltaTime;
        if(velocity[1] <= TAKO_MAX_DOWNWARD_Y_VELOCITY) {
            velocity[1] = TAKO_MAX_DOWNWARD_Y_VELOCITY;
        }
        pos[0] += velocity[0] * deltaTime;
        pos[1] += velocity[1] * deltaTime;
        if(previousVelocityX != velocity[0]) {
            float rightMsPressed = controller->get_right_ms_pressed_and_clear();
            float leftMsPressed = controller->get_left_ms_pressed_and_clear();
            pos[0] += TAKO_SPEED * ((rightMsPressed - leftMsPressed) / 1000.0f);
        }
        if(pos[0] <= 0.0f) {
            pos[0] = 0.0f;
        }
        if(pos[0] >= ARENA_SIZE_X - TAKO_SIZE) {
            pos[0] = ARENA_SIZE_X - TAKO_SIZE;
        }
        if(pos[1] <= 0.0f) {
            pos[1] = 0.0f;
            velocity[1] = 0.0f;
        }
    }
    return true;
}

void Tako::down_pressed(float durationSincePress) {
    if(pos[1] > 0.0f && durationSincePress > 0.0f && velocity[1] > TAKO_MAX_DOWNWARD_Y_VELOCITY) {
        velocity[1] = TAKO_MAX_DOWNWARD_Y_VELOCITY;
        pos[1] += velocity[1] * durationSincePress;
        if(pos[1] <= 0.0f) {
            pos[1] = 0.0f;
            velocity[1] = 0.0f;
        }
    }
}

void Tako::up_pressed(float durationSincePress, float typicalDelta) {
    if(pos[1] == 0.0f && durationSincePress > 0.0f) {
        velocity[1] = TAKO_JUMP_VELOCITY;
        for(float i = 0; i <= durationSincePress; i += typicalDelta) {
            velocity[1] -= GRAVITY * typicalDelta;
            if(velocity[1] <= TAKO_MAX_DOWNWARD_Y_VELOCITY) {
                velocity[1] = TAKO_MAX_DOWNWARD_Y_VELOCITY;
            }
            pos[1] += velocity[1] * typicalDelta;
            if(pos[1] <= 0.0f) {
                pos[1] = 0.0f;
                velocity[1] = 0.0f;
                break;
            }
        }
    }
}

const std::shared_ptr<GamePlayer>& Tako::get_controller() const {
    return controller;
}

void Tako::write_new(std::vector<uint8_t>& dataBuffer) {
    WorldObject::write_new(dataBuffer);
    write_common(dataBuffer);
    write_short_string_to_buffer(dataBuffer, controller->get_name());
}

void Tako::write_update(std::vector<uint8_t>& dataBuffer) {
    WorldObject::write_update(dataBuffer);
    write_common(dataBuffer);
}

void Tako::mark_to_melt() {
    timeSinceMarked = 0.01;
}

void Tako::write_common(std::vector<uint8_t>& dataBuffer) {
    if(timeAfterBonk > 0.0 && !melting) {
        dataBuffer.emplace_back('b');
    }
    else if(timeAfterBonk > 0.0 && melting) {
        dataBuffer.emplace_back('m');
    }
    else if(timeWithoutCrowbar == 0.0 && timeSinceMarked > 0.0) {
        dataBuffer.emplace_back('x');
    }
    else if(timeWithoutCrowbar == 0.0 && timeSinceMarked == 0.0) {
        dataBuffer.emplace_back('w');
    }
    else if(timeSinceMarked > 0.0) {
        dataBuffer.emplace_back('u');
    }
    else {
        dataBuffer.emplace_back('t');
    }
    write_integer_to_buffer<int16_t>(dataBuffer, static_cast<int16_t>(pos[0]));
    write_integer_to_buffer<int16_t>(dataBuffer, static_cast<int16_t>(pos[1]));
    write_integer_to_buffer<int16_t>(dataBuffer, static_cast<int16_t>(velocity[0]));
    write_integer_to_buffer<int16_t>(dataBuffer, static_cast<int16_t>(velocity[1]));
}

Tako::~Tako() { }
