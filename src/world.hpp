#pragma once
#include <deque>
#include <memory>
#include "tako.hpp"
#include "crowbar.hpp"
#include "tictactoe.hpp"

class World {
public:
    void update(double deltaTime);
    bool update_scoreboard(const std::deque<std::shared_ptr<GamePlayer>>& players);
    bool update_queue_message();
    void write_new(std::vector<uint8_t>& dataBuffer);
    void write_update(std::vector<uint8_t>& dataBuffer);
    std::list<Tako> takos;
    std::list<Crowbar> crowbars;
    std::deque<std::shared_ptr<GamePlayer>> queueForTako;
    TicTacToe tictactoe;

    bool doQueueUpdate = true;
    std::shared_ptr<std::vector<uint8_t>> queueMessageBuffer = std::make_shared<std::vector<uint8_t>>();
    std::shared_ptr<std::vector<uint8_t>> scoreboardMessageBuffer = std::make_shared<std::vector<uint8_t>>();
};
