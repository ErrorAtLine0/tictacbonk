#pragma once
#include <climits>
#include <vector>
#include <cstdint>
#include <string>
#include <random>
#include <stdexcept>

class RNG {
public:
    template <typename T> static T get_random_integer(T firstNumber, T lastNumber) {
        std::uniform_int_distribution<T> dist(firstNumber, lastNumber);
        return dist(mt);
    }
    template <typename T> static T get_random_real_number(T firstNumber, T lastNumber) {
        std::uniform_real_distribution<T> dist(firstNumber, lastNumber);
        return dist(mt);
    }
private:
    static std::random_device rd;
    static std::mt19937 mt;
};

bool is_big_endian();

// From: https://stackoverflow.com/a/4956493
template <typename T>
T swap_endian(T u)
{
    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union
    {
        T u;
        unsigned char u8[sizeof(T)];
    } source, dest;

    source.u = u;

    for (std::size_t k = 0; k < sizeof(T); k++)
        dest.u8[k] = source.u8[sizeof(T) - k - 1];

    return dest.u;
}

template <typename T> void write_integer_to_buffer(std::vector<uint8_t>& dataBuffer, T integerToWrite) {
    union {
        T u;
        uint8_t u8[sizeof(T)];
    } source;
    source.u = integerToWrite;

    if(is_big_endian()) {
        for(unsigned i = 0; i < sizeof(T); i++) {
            dataBuffer.emplace_back(source.u8[i]);
        }
    }
    else {
        for(unsigned i = 0; i < sizeof(T); i++) {
            dataBuffer.emplace_back(source.u8[sizeof(T) - i - 1]);
        }
    }
}

template <typename T> T clamp_value(T val, T min, T max) {
    if(val < min) {
        return min;
    }
    else if(val > max) {
        return max;
    }
    return val;
}

std::string read_short_string_from_string(const std::string& dataBuffer, unsigned& indexToReadAt);
void write_short_string_to_buffer(std::vector<uint8_t>& dataBuffer, const std::string& strToWrite);

template <typename T> T read_integer_from_string(const std::string& dataBuffer, unsigned& indexToReadAt) {
    if(indexToReadAt + sizeof(T) > dataBuffer.size()) {
        throw std::out_of_range("Integer size to read is out of bounds");
    }
    T storedInteger = *reinterpret_cast<const T*>(dataBuffer.data() + indexToReadAt);
    indexToReadAt += sizeof(T);
    if(is_big_endian()) {
        return storedInteger;
    }
    else {
        return swap_endian<T>(storedInteger);
    }
}
