#include "worldobject.hpp"

std::unordered_set<uint8_t> WorldObject::reservedWorldIDs;
std::unordered_map<uint8_t, std::chrono::steady_clock::time_point> WorldObject::timeSinceDestruction;
uint8_t WorldObject::nextIDToAssign = 0;
const double WorldObject::secFromDestructionToFreeID = 7.0;

WorldObject::WorldObject(): newObject(true) {
    while(reservedWorldIDs.count(nextIDToAssign) || is_temporary_reserved(nextIDToAssign)) {
        nextIDToAssign++;
    }
    reservedWorldIDs.emplace(nextIDToAssign);
    worldObjectID = nextIDToAssign;
}

WorldObject::~WorldObject() {
    reservedWorldIDs.erase(worldObjectID);
    timeSinceDestruction[worldObjectID] = std::chrono::steady_clock::now();
}

bool WorldObject::is_temporary_reserved(uint8_t idToCheck) {
    auto it = timeSinceDestruction.find(idToCheck);
    if(it == timeSinceDestruction.end()) {
        return false;
    }
    return std::chrono::duration<double>(std::chrono::steady_clock::now() - it->second).count() < secFromDestructionToFreeID;
}

void WorldObject::write_new(std::vector<uint8_t>& dataBuffer) {
    dataBuffer.emplace_back(worldObjectID);
}

void WorldObject::write_update(std::vector<uint8_t>& dataBuffer) {
    dataBuffer.emplace_back(worldObjectID);
}
