A multiplayer online browser-based fighting fangame based on [Ninomae Ina'nis](https://www.youtube.com/channel/UCMwGHR0BTZuLsmjY_NT5Pwg).

The project's license is found in the LICENSE file.

Licenses of libraries used in the project can be found in the other_licenses folder.
